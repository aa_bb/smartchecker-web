﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Controls
{
    public partial class MyRoleSidebar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e) {}

        protected string GetMyRole()
        {
            if (HttpContext.Current.Session["UserRole"] != null)
            {
                string levelCode = HttpContext.Current.Session["UserRole"].ToString();
                string role;
                if (levelCode == "1")
                    role = "Студент";
                else if (levelCode == "2")
                    role = "Учитель";
                else
                    role = "Admin";
                return string.Format("<a>{0}</a>", role);
            }
            return "<a>Гость</a>";
        }

        protected void Logout_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Session["UserId"] = null;
            HttpContext.Current.Session["UserRole"] = null;
            HttpContext.Current.Response.Redirect("/login");
        }
    }
}