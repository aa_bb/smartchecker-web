﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StudentHeader.ascx.cs" Inherits="_SmartCheckerWeb.Controls.Headers.StudentHeader" %>
<%@ Register TagPrefix="SC" TagName="MyRoleSidebar" Src="~/Controls/Headers/MyRoleSidebar.ascx" %>
 <style type='text/css'>
     @import url('../../Content/Styles.css');
 </style>
    <ul>
        <li><a href="#"><img src="../../Content/logo.png" class="logo"/></a></li>
        <li><a href="#">Обучение</a></li>
        <li><a href="check-solutions">Проверить решение</a></li>
        <li><a href="my-solutions">Мои решения</a></li>
        <li><a href="check-solutions">Мои задачи</a></li>
    </ul>

    <SC:MyRoleSidebar runat="server"/>
