﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TeacherHeader.ascx.cs" Inherits="_SmartCheckerWeb.Controls.TeacherHeader" %>
<%@ Register TagPrefix="SC" TagName="MyRoleSidebar" Src="~/Controls/Headers/MyRoleSidebar.ascx" %>
 <style type='text/css'>
     @import url('../../Content/Styles.css');
 </style>
    <ul>
        <li><a href="#"><img src="../../Content/logo.png" class="logo"/></a></li>
        <li><a href="#">Обучение</a></li>
        <li><a href="my-tasks">Мои задачи</a></li>
        <li><a href="my-students">Мои студенты</a></li>
        <li><a href="task-statistic">Статистика</a></li>
        <li><a href="my-colleague">Мои коллеги</a></li>
        <li><a href="constructor">Конструктор</a></li>
    </ul>

    <SC:MyRoleSidebar runat="server"/>
