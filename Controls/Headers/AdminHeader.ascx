﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminHeader.ascx.cs" Inherits="_SmartCheckerWeb.Controls.AdminHeader" %>
<%@ Register TagPrefix="SC" TagName="MyRoleSidebar" Src="~/Controls/Headers/MyRoleSidebar.ascx" %>
 <style type='text/css'>
     @import url('../../Content/Styles.css');
 </style>
    <ul>
        <li><a href="#"><img src="../../Content/logo.png" class="logo"/></a></li>
        <li><a href="task-manager">Задачи</a></li>
        <li><a href="users">Пользователи</a></li>
        <li><a href="constructor">Конструктор</a></li>
        <li><a href="plugin-manager">Менеджер плагинов</a></li>
        <li><a href="task-statistic">Статистика</a></li>
    </ul>
    <SC:MyRoleSidebar runat="server"/>
