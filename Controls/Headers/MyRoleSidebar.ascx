﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyRoleSidebar.ascx.cs" Inherits="_SmartCheckerWeb.Controls.MyRoleSidebar" %>

<span class="sidebar">
    <span class="role">Моя роль:
            <%= GetMyRole() %>
    </span>
    <span class="logout">
        <asp:LinkButton id="btnLogout" Text="Выйти" runat="server" OnClick="Logout_Click" />
    </span>
</span>
