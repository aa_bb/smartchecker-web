﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages.Teacher
{
    public partial class TaskStatistic : System.Web.UI.Page
    {
        protected Repository repository = new Repository();
        protected Dictionary<int, List<Stat>> taskInfoDict;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AuthUtils.CheckAuth("task-statistic", Context, 2);
                int userId = int.Parse(Session["UserId"].ToString());
                IEnumerable<Stat> myStat = repository.Stats.Where(stat => stat.TaskInfo.TaskOwners.Where(to => to.UserId == userId).Any());
                taskInfoDict = new Dictionary<int, List<Stat>>();
                foreach (var s in myStat)
                {
                    if (!taskInfoDict.ContainsKey(s.TaskInfoId))
                        taskInfoDict[s.TaskInfoId] = new List<Stat>();
                    taskInfoDict[s.TaskInfoId].Add(s);
                }
            }
        }

        protected IEnumerable<Stat> GetStatistic(int taskInfoId)
        {
            // исключить ранние попытки, вернуть только лучшие
            Dictionary<int, List<Stat>> attemptDict = new Dictionary<int, List<Stat>>();
            List<Stat> finalStat = new List<Stat>();
            
            
            foreach(Stat stat in taskInfoDict[taskInfoId])
            {
                if (!attemptDict.ContainsKey(stat.UserId))
                    attemptDict.Add(stat.UserId, new List<Stat>());
                attemptDict[stat.UserId].Add(stat);   
            }

            foreach (var userAttempts in attemptDict.Keys)
            {
                Stat maxAttempt = attemptDict[userAttempts].Aggregate((i, j) => i.Result > j.Result ? i : j);
                finalStat.Add(maxAttempt);
            } 
            return finalStat;
        }
    }
}