﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages
{
    public partial class MyStudents : System.Web.UI.Page
    {
        private Repository repository = new Repository();
        
        static List<User> allStudents;
        static List<User> myStudents;
        static int userId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AuthUtils.CheckAuth("my-students", Context, 2);
                userId = int.Parse(Session["UserId"].ToString());
                allStudents = GetStudents();
                myStudents = GetMyStudents();
                allStudents = allStudents.Except(myStudents).ToList();
            }
            FillAllStudentsTable();
            FillMyStudentsTable();
        }

        private void FillAllStudentsTable()
        {
            HtmlTableRow header = studentsTable.Rows[0];
            studentsTable.Rows.Clear();
            studentsTable.Rows.Add(header);
            foreach (var student in allStudents)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableUtils.AddCell(row, student.Surname);
                HtmlTableUtils.AddCell(row, student.Name);

                HtmlTableUtils.AddBtnCell(row, "Добавить", student.Id.ToString(), AddStudentToMy_Click);
                studentsTable.Rows.Add(row);
            }
        }

        private void FillMyStudentsTable()
        {
            HtmlTableRow header = myStudentsTable.Rows[0];
            myStudentsTable.Rows.Clear();
            myStudentsTable.Rows.Add(header);
            foreach (var student in myStudents)
                AddMyStudentsRow(student);
        }

        private void AddMyStudentsRow(User student)
        {
            HtmlTableRow row = new HtmlTableRow();
            HtmlTableUtils.AddCell(row, student.Surname);
            HtmlTableUtils.AddCell(row, student.Name);
            HtmlTableUtils.AddBtnCell(row, "Изменить роль до преподавателя", student.Id.ToString(), UpdateStudentRole_Click);
            myStudentsTable.Rows.Add(row);
        }

        protected void AddStudentToMy_Click(object sender, EventArgs e) { }

        protected List<User> GetMyStudents()
        {
            //мой студент это такой студент, у которого есть доступ ко всем моим задачам
            IEnumerable<int> ownerTaskIds = repository.FindUser(userId).TaskOwners.Select(to => to.TaskInfoId);
            return allStudents.Where(
                user => user.TaskOwners
                .Select(to => to.TaskInfoId)
                .SequenceEqual(ownerTaskIds))
                .Where(u => u.Id != userId)
                .ToList();
        }

        protected List<User> GetStudents()
        {
            return repository.Users.Where(user => user.Role.LevelCode == 1 && user.Id != userId).ToList();
        }

        protected void UpdateStudentRole_Click(object sender, CommandEventArgs e)
        {
            int selectedId = int.Parse(e.CommandArgument.ToString());
            int index = myStudents.TakeWhile(u => u.Id != selectedId).Count();
            var updateStudent = myStudents[index];
            myStudents.RemoveAt(index);
            myStudentsTable.Rows.RemoveAt(index + 1);
            repository.UpdateRole(updateStudent.Id, updateStudent.Role.LevelCode);
        }
        
        protected void AddStudentToMy_Click(object sender, CommandEventArgs e)
        {
            int selectedId = int.Parse(e.CommandArgument.ToString());
            int index = allStudents.TakeWhile(u => u.Id != selectedId).Count();
            var newStudent = allStudents[index];
            myStudents.Add(newStudent);
            allStudents.RemoveAt(index);
            List<TaskOwner> userTaskOwners = repository.FindUser(userId).TaskOwners;
            foreach (var to in userTaskOwners)
                repository.SaveTaskOwner(to.TaskInfoId, newStudent.Id);
            AddMyStudentsRow(newStudent);
            studentsTable.Rows.RemoveAt(index + 1);
        }
    }
}