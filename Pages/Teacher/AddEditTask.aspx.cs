﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages.Teacher
{
    public partial class AddUpdateTask : System.Web.UI.Page
    {
        Repository repository = new Repository();
        static TaskInfo taskInfo;
        static List<Task> tasks;
        static string mode;
        static string descriptionFilename;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AuthUtils.CheckAuth(Request.Url.AbsoluteUri, Context, 2);
                if (Request.QueryString["Mode"] == null)
                    return;
                mode = Request.QueryString["Mode"].ToString();
                setTimeLimit.Visible = false;

                if ("edit".Equals(mode))
                {
                    if (Request.QueryString["Task"] == null)
                        return;
                    int taskInfoId = int.Parse(Request.QueryString["Task"].ToString());
                    taskInfo = repository.FindTaskInfo(taskInfoId);
                    txtdescr.Text = taskInfo.TaskDescriptionText;
                    txtName.Text = taskInfo.Name;
                    descriptionFilename = taskInfo.TaskDescriptionFilePath;
                    if (!string.IsNullOrEmpty(taskInfo.TaskDescriptionFilePath))
                        lblFileUpload.Text = lblFileUpload.Text + taskInfo.TaskDescriptionFilePath;
                    else
                        lblFileUpload.Visible = false;
                    txtTimeLimit.Text = taskInfo.DefaultTimeLimit.ToString();

                    tasks = repository.Tasks.Where(t => t.TaskInfoId == taskInfo.Id).ToList();
                }
                else
                {
                    taskInfo = new TaskInfo();
                    lblFileUpload.Visible = false;
                    tasks = new List<Task>();
                }
            }
            FillTable();
        }

        private void FillTable()
        {
            HtmlTableRow header = taskTable.Rows[0];
            taskTable.Rows.Clear();
            taskTable.Rows.Add(header);
            foreach (var task in tasks)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableUtils.AddCell(row, task.Test.Name);
                HtmlTableUtils.AddCell(row, task.Pattern.ToString());
                HtmlTableUtils.AddCell(row, task.TimeLimit.ToString());
                HtmlTableUtils.AddBtnCell(row, "X", task.Test.Name, DeleteTestBtn_Click);
                HtmlTableUtils.AddBtnCell(row, "Изменить лимит времени", task.Test.Name, SetTimeLimitStart_Click);
                if (mode == "edit")
                    HtmlTableUtils.AddBtnCell(row, "Изменить шаблон", task.PatternId.ToString(), EditPattern_Click);
                taskTable.Rows.Add(row);
            }
        }
        protected void LoadDescrFile_Click(object sender, EventArgs e)
        {
            string filename = Path.GetFileName(FileUpload1.FileName);
            if (string.IsNullOrEmpty(filename))
                return;
            string path = Server.MapPath(FilePathHelper.DescriptionPath) + filename;
            FileUpload1.SaveAs(path);
            lblFileUpload.Text += filename;
            lblFileUpload.Visible = true;
            descriptionFilename = path;
        }

        protected void DeleteTestBtn_Click(object sender, CommandEventArgs e) {
            string testName = e.CommandArgument.ToString();
            int index = tasks.FindIndex(t => t.Test.Name == testName);
            tasks.RemoveAt(index);
            taskTable.Rows.RemoveAt(index + 1);
            FilePathHelper.DeleteTestFile(testName, Server, taskInfo.Id);
        }

        protected void LoadTestFileBtn_Click(object sender, EventArgs e) {
            string filename = Path.GetFileName(FileUpload2.FileName);
            if (string.IsNullOrEmpty(filename))
                return;
            if (tasks.Where(t => t.Test.Name == filename).Any()) {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Ошибка", "alert('Тест с таким именем уже существует')", true);
                return;
            }
            
            FileUpload2.SaveAs(Server.MapPath(FilePathHelper.TestsPath) + filename);
            Task newTask = new Task() { Pattern = new Pattern(), Test = new Test() { Name = filename }, TimeLimit = taskInfo.DefaultTimeLimit };
            tasks.Add(newTask);

            FillTable();
        }

        protected void EditPattern_Click(object sender, CommandEventArgs e)
        {
            HttpContext.Current.Response.Redirect("add-edit-pattern?Arg=" + e.CommandArgument);
        }

        protected void SetTimeLimitStart_Click(object sender, CommandEventArgs e)
        {
            setTimeLimit.Visible = true;
            lblTestName.Text = e.CommandArgument.ToString();
            txtSetTimeLimit.Text = "";
        }
        
        protected void SetTimeLimitFinish_Click(object sender, EventArgs e)
        {
            string testname = lblTestName.Text;
            int index = tasks.TakeWhile(t => t.Test.Name != testname).Count();
            tasks[index].TimeLimit = int.Parse(txtSetTimeLimit.Text);
            ((Label) taskTable.Rows[index + 1].Cells[2].Controls[0]).Text = txtSetTimeLimit.Text;
            setTimeLimit.Visible = false;
        }

        protected void SaveTask_Click(object sender, EventArgs e) {
            taskInfo.TaskDescriptionText = txtdescr.Text;
            taskInfo.Name = txtName.Text;
            if (!string.IsNullOrEmpty(txtTimeLimit.Text))
                taskInfo.DefaultTimeLimit = int.Parse(txtTimeLimit.Text);

            FilePathHelper.DeleteOldDescriptionFile(descriptionFilename);
            
            if ("add".Equals(Request.QueryString["Mode"].ToString()))
                taskInfo.Key = "10";
            repository.SaveTaskInfo(taskInfo);
            string newDescriptionFilePath = "";
            if (!string.IsNullOrEmpty(descriptionFilename))
                newDescriptionFilePath = FilePathHelper.MoveDescriptionFile(descriptionFilename, taskInfo.Id, Server);
            taskInfo.TaskDescriptionFilePath = newDescriptionFilePath;
            repository.SaveTaskInfo(taskInfo);

            foreach (var task in tasks)
            {
                task.TaskInfoId = taskInfo.Id;
                repository.SaveTask(task);
                FilePathHelper.RelocateTest(task.Test.Name, taskInfo.Id, Server);
            }
            if ("add".Equals(Request.QueryString["Mode"].ToString()))
                repository.AddTaskOwner(int.Parse(Session["UserId"].ToString()), taskInfo.Id);    
            
            HttpContext.Current.Response.Redirect("my-tasks");
        }

        protected void txtTimeLimit_TextChanged(object sender, EventArgs e)
        {
            int timeLimit;
            if (int.TryParse(txtTimeLimit.Text, out timeLimit))
                taskInfo.DefaultTimeLimit = timeLimit;
        }
    }
}