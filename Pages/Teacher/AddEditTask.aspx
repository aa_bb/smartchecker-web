﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditTask.aspx.cs" Inherits="_SmartCheckerWeb.Pages.Teacher.AddUpdateTask"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">
        <h3><a href="#"><%="edit".Equals(Request.QueryString["Mode"].ToString()) ? "Обновление задачи" : "Создание задачи"%></a></h3>
        <br />
        <br />

        <asp:Label ID="labelName" AssociatedControlID="txtName" Text="Название:" runat="server" />
        <asp:TextBox ID="txtName" runat="server" class="textInput" />
        <br />
        <br />

        <asp:Label ID="labelDescr" AssociatedControlID="txtDescr" Text="Условие задачи:" runat="server" /><br />
        <asp:TextBox ID="txtdescr" class="textInput" Rows="8" TextMode="multiline" runat="server" />
        <br />
        <br />


        <div>
            <asp:Label ID="lblFileUpload" Text="Загруженный файл условия: " runat="server" /><br />
            <asp:FileUpload class="btn btn-info" ID="FileUpload1" runat="server" />
            <asp:Button ID="UploadSolutionBtn" class="btn btn-info" runat="server" Text="Загрузить файл" OnClick="LoadDescrFile_Click" /><br />
        </div>
        <br />
        <br />

        <asp:Label ID="lblTimeLimit" AssociatedControlID="txtTimeLimit" Text="Лимит по времени по умолчанию:" runat="server" />
        <asp:TextBox ID="txtTimeLimit" OnTextChanged="txtTimeLimit_TextChanged" class="textInput" runat="server" />
        <br />
        <br />
        <br />
        <div>
            <asp:FileUpload class="btn btn-info" ID="FileUpload2" runat="server" />
            <asp:Button class="btn btn-info" ID="LoadFileBtn" runat="server" OnClick="LoadTestFileBtn_Click" Text="Загрузить файл теста" />
        </div>
        <br />

        <h3><a href="#">Тесты</a></h3>

        <table class="styled-table" id="taskTable" runat="server">
            <thead>
                <tr>
                    <th>Имя</th>
                    <th>Шаблон</th>
                    <th>Лимит времени</th>
                    <th></th>
                </tr>
            </thead>
        </table>

        <div id="setTimeLimit" class="centered_infobox" runat="server">
            <asp:Label ID="lblTestName" color="white" runat="server" />
            <div class="inner_infobox">
                <asp:Label ID="lblSetTimeLimit" AssociatedControlID="txtSetTimeLimit" Text="Лимит по времени:" runat="server" />
                <asp:TextBox ID="txtSetTimeLimit" class="textInput" runat="server" />
                <asp:Button class="btn btn-info" ID="SetTimeLimitBtn" runat="server" OnClick="SetTimeLimitFinish_Click" Text="Применить" />
            </div>
        </div>
        <br />
        <br />

        <asp:Button ID="SaveBtn" Text="Сохранить" OnClick="SaveTask_Click" class="btn btn-info widebtn" runat="server" />
    </div>
</asp:Content>
