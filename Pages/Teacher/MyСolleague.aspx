﻿    <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyСolleague.aspx.cs" Inherits="_SmartCheckerWeb.Pages.Teacher.MyСolleague" 
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">
        <h3><a href="#">Мои Коллеги</a></h3>
        <table  class="styled-table" id="myColleagueTable" runat="server">
            <thead>
                <tr>
                    <th>Фамилия</th>
                    <th>Имя</th>
                </tr>
            </thead>
        </table>

        <br />
        <h3><a href="#">Добавить коллег в мою группу</a></h3>

        <table class="styled-table" id="colleagueTable" runat="server">
            <thead>
                <tr>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</asp:Content>

