﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages.Teacher
{
    public partial class MyСolleague : System.Web.UI.Page
    {
        Repository repository = new Repository();
        static List<User> allColleague;
        static List<User> myColleague;
        static int userId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AuthUtils.CheckAuth("my-colleague", Context, 2);
                userId = int.Parse(Session["UserId"].ToString());
                allColleague = GetColleague();
                myColleague = GetMyColleague();
                allColleague = allColleague.Except(myColleague).ToList();
            }
            FillAllColleagueTable();
            FillMyColleagueTable();
        }

        private void FillAllColleagueTable()
        {
            HtmlTableRow header = colleagueTable.Rows[0];
            colleagueTable.Rows.Clear();
            colleagueTable.Rows.Add(header);
            foreach (var colleague in allColleague)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableUtils.AddCell(row, colleague.Surname);
                HtmlTableUtils.AddCell(row, colleague.Name);
                
                HtmlTableUtils.AddBtnCell(row, "Добавить", colleague.Id.ToString(), AddColleagueToMy_Click);
                colleagueTable.Rows.Add(row);
            }
        }

        private void FillMyColleagueTable()
        {
            HtmlTableRow header = myColleagueTable.Rows[0];
            myColleagueTable.Rows.Clear();
            myColleagueTable.Rows.Add(header);
            foreach (var colleague in myColleague)
                AddMyColleagueRow(colleague);
        }

        private void AddMyColleagueRow(User colleague)
        {
            HtmlTableRow row = new HtmlTableRow();
            HtmlTableUtils.AddCell(row, colleague.Surname);
            HtmlTableUtils.AddCell(row, colleague.Name);
            myColleagueTable.Rows.Add(row);
        }

        protected List<User> GetMyColleague()
        {
            //мой коллега это такой коллега, у которого есть доступ ко всем моим задачам
            IEnumerable<int> ownerTaskIds = repository.FindUser(userId).TaskOwners.Select(to => to.TaskInfoId);
            return allColleague.Where(
                user => user.TaskOwners
                .Select(to => to.TaskInfoId)
                .SequenceEqual(ownerTaskIds))
                .Where(u => u.Id != userId)
                .ToList();
        }

        protected List<User> GetColleague()
        {
            return repository.Users.Where(user => user.Role.LevelCode == 2 && user.Id != userId).ToList();
        }

        protected void AddColleagueToMy_Click(object sender, CommandEventArgs e)
        {
            int selectedId = int.Parse(e.CommandArgument.ToString());
            int index = allColleague.TakeWhile(u => u.Id != selectedId).Count();
            var newColleague = allColleague[index];
            myColleague.Add(newColleague);
            allColleague.RemoveAt(index);
            List<TaskOwner> userTaskOwners = repository.FindUser(userId).TaskOwners;
            foreach (var to in userTaskOwners)
                repository.SaveTaskOwner(to.TaskInfoId, newColleague.Id);
            AddMyColleagueRow(newColleague);
            colleagueTable.Rows.RemoveAt(index + 1);
        }
    }
}