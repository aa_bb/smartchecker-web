﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using _SmartCheckerWeb.Pages.Helpers.EmbeddedPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages.Teacher
{
    public partial class AddEditPattern : System.Web.UI.Page
    {
        Repository repository = new Repository();
        string NotSetStr = new EmptyPattern().Name;
        static Pattern pattern;

        static List<Pattern> seqPatternList = new List<Pattern>();
        static int seqAnswerClassId;
        static List<HtmlTableRow> combiPatternRowsList;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AuthUtils.CheckAuth(Request.Url.AbsoluteUri, Context, 2);
                if (Request.QueryString["Arg"] == null)
                    return;

                int patternId = int.Parse(Request.QueryString["Arg"].ToString());
                List<string> dataSource = new List<string>() { NotSetStr };
                dataSource.AddRange(
                    repository.AnswerClasses
                    .Where(ac => ac.Plugin.Enabled)
                    .Select((el) => { return el.ToString(); })
                    .Distinct()
                    );
                PatternDropDownList.DataSource = dataSource;
                PatternDropDownList.DataBind();
                combiPatternRowsList = new List<HtmlTableRow>();
                pattern = repository.FindPattern(patternId);
                if (pattern.AnswerClass != null)
                    PatternDropDownList.SelectedIndex = dataSource.IndexOf(pattern.AnswerClass.Name);
            }

            if (PatternDropDownList.SelectedIndex != 0)
                GUIChanger(GetIPatternFromDropDownList());
            else
                SetPageEmpty();
            
        }

        private void SetPageEmpty()
        {
            plainGenPattern.Visible = false;
            combiPattern.Visible = false;
            sequencePattern.Visible = false;
        }

        protected void PatternDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PatternDropDownList.SelectedValue.Equals(NotSetStr))
                return;
            var iPattern = GetIPatternFromDropDownList();
            GUIChanger(iPattern);
        }

        IPattern GetIPatternFromDropDownList()
        {
            string answerClassName = PatternDropDownList.SelectedValue;
            var answerClass = repository.AnswerClasses.Where(ac => ac.Name.Equals(answerClassName)).First();
            return AnswerClassesAlgoHelper.GetAnswerClassAlgo(answerClass);
        }

        private void GUIChanger(IPattern iPattern)
        {
            SetPageEmpty();
            if (PatternInterfaceHelper.IsPlainPattern(iPattern) || PatternInterfaceHelper.IsGenPattern(iPattern))
                PlainGenPatternSelected();
            else if (PatternInterfaceHelper.IsCombiPattern(iPattern))
                CombiPatternSelected((ICombiPattern)iPattern);
            else if (PatternInterfaceHelper.IsSequencePattern(iPattern))
                SeqPatternSelected((ISequencePattern)iPattern);
        }

        void PlainGenPatternSelected()
        {
            plainGenPattern.Visible = true;
            if (!IsPostBack)
                txtBasicAnswer.Text = pattern.BasicAnswer;
        }

        void CombiPatternSelected(ICombiPattern combi)
        {
            combiPattern.Visible = true;
            List<Pattern> combiPatternList = new List<Pattern>();
            int? nextPatternId = pattern.NextOuterPatternId;
            if (nextPatternId == null) // создаем объекты при смене класса
            {
                List<IPattern> answerClasses = combi.PatternList;
                for (int i = 0; i < answerClasses.Count; i++)
                {
                    string delimiter = (i == answerClasses.Count - 1) ? null : @"\r\n";
                    AnswerClass answerClass = repository.AnswerClasses.Where(ac => ac.Name == answerClasses[i].Name).First();
                    Pattern curPattern = new Pattern() { AnswerClass = answerClass, BasicAnswer = null, Delimiter = delimiter };
                    combiPatternList.Add(curPattern);
                }
            }
            else
            {
                // достаем из БД при загрузке формы
                while (nextPatternId != null)
                {
                    Pattern curPattern = repository.FindPattern(nextPatternId);
                    combiPatternList.Add(curPattern);
                    nextPatternId = curPattern.NextOuterPatternId;
                }
            }

            FillTableCombi(combiPatternList);
        }

        void FillTableCombi(List<Pattern> combiPatternList)
        {
            HtmlTableRow header = combiPatternTable.Rows[0];
            combiPatternTable.Rows.Clear();
            combiPatternTable.Rows.Add(header);

            if (combiPatternRowsList.Count != 0)
            {
                foreach (var patternRow in combiPatternRowsList)
                    combiPatternTable.Rows.Add(patternRow);
            }
            else
            {
                foreach (var pattern in combiPatternList)
                {
                    HtmlTableRow row = new HtmlTableRow();
                    HtmlTableUtils.AddCell(row, pattern.AnswerClass.Name);
                    HtmlTableUtils.AddTextCell(row, pattern.BasicAnswer);
                    HtmlTableUtils.AddTextCell(row, pattern.Delimiter);
                    combiPatternTable.Rows.Add(row);
                    combiPatternRowsList.Add(row);
                }
            }
        }

        void OpenOkBtn(object sender, EventArgs e)
        {
            if (PatternDropDownList.SelectedIndex == 0)
            {
                OkBtn.Visible = false;
                return;
            }
            IPattern patternImpl = GetIPatternFromDropDownList();
            if (PatternInterfaceHelper.IsPlainPattern(patternImpl))
                OkBtn.Visible = !string.IsNullOrEmpty(txtBasicAnswer.Text);
            else if (PatternInterfaceHelper.IsCombiPattern(patternImpl))
                OkBtn.Visible = CombiPatternOpenOKCondition();
            else if (PatternInterfaceHelper.IsGenPattern(patternImpl))
            {
                bool notEmptyBAGen = !string.IsNullOrEmpty(txtBasicAnswer.Text);
                OkBtn.Visible = notEmptyBAGen;
            }
            else if (PatternInterfaceHelper.IsSequencePattern(patternImpl))
            {
                bool notEmptyBASeq = sequenceAnswersTable.Rows.Count != 1;
                OkBtn.Visible = notEmptyBASeq;
            }
            else
                OkBtn.Visible = false;
        }

        bool CombiPatternOpenOKCondition()
        {
            bool notEmptyBACombi = true;
            for (int i = 1; notEmptyBACombi && i < combiPatternTable.Rows.Count; i++)
            {
                object value = ((Label)combiPatternTable.Rows[i].Cells[1].Controls[0]).Text;
                if (value == null || string.IsNullOrEmpty(value.ToString()))
                    notEmptyBACombi = false;
            }

            bool notEmptyDelimiter = true;
            for (int i = 1; notEmptyDelimiter && i < combiPatternTable.Rows.Count - 1; i++)
            {
                object value = ((Label)combiPatternTable.Rows[i].Cells[2].Controls[0]).Text;
                if (value == null || string.IsNullOrEmpty(value.ToString()))
                    notEmptyBACombi = false;
            }
            return notEmptyBACombi && notEmptyDelimiter;
        }

        void SeqPatternSelected(ISequencePattern sequence)
        {
            sequencePattern.Visible = true;
            int? nextInnerPatternId = pattern.NextInnerPatternId;
            if (nextInnerPatternId == null) // создание новых объектов
            {
                sequence.PatternList = new List<IPlainPattern>();
                seqAnswerClassId = repository.AnswerClasses.Where(anc => anc.Name.Equals("Точное совпадение")).First().Id;
            }
            else // подгрузка из БД
            {
                while (nextInnerPatternId != null)
                {
                    Pattern curPattern = repository.FindPattern(nextInnerPatternId);
                    seqPatternList.Add(curPattern);
                    nextInnerPatternId = curPattern.NextInnerPatternId;
                }
            }

            FillTableSeq();
        }

        void FillTableSeq()
        {
            HtmlTableRow header = sequenceAnswersTable.Rows[0];
            sequenceAnswersTable.Rows.Clear();
            sequenceAnswersTable.Rows.Add(header);
            for (int i = 0; i < seqPatternList.Count; i++)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableUtils.AddCell(row, seqPatternList[i].BasicAnswer);
                HtmlTableUtils.AddBtnCell(row, "Удалить", i.ToString(), DelBtn_Click);
                sequenceAnswersTable.Rows.Add(row);
            }
        }

        void DeleteOldPatternRecords(Pattern dbPattern)
        {
            dbPattern.BasicAnswer = null;
            repository.DeleteOldPatternRecords(dbPattern);
        }

        protected void Ok_Click(object sender, EventArgs e)
        {
            if (PatternDropDownList.SelectedIndex == 0)
            {
                int tiId = repository.GetTaskInfoByPattern(pattern.Id);
                HttpContext.Current.Response.Redirect("add-edit-task?Mode=edit&Task=" + tiId);
                return;
            }
            var dbPattern = repository.FindPattern(pattern.Id);
            DeleteOldPatternRecords(dbPattern);
            dbPattern.AnswerClassId = repository.AnswerClasses.Where(ac => ac.Name.Equals(PatternDropDownList.SelectedValue)).First().Id;

            IPattern patternImpl = GetIPatternFromDropDownList();
            if (PatternInterfaceHelper.IsPlainPattern(patternImpl))
                SavePlainPattern(dbPattern);
            else if (PatternInterfaceHelper.IsCombiPattern(patternImpl))
                SaveCombiPattern(dbPattern);
            else if (PatternInterfaceHelper.IsGenPattern(patternImpl))
                SaveGenPattern(dbPattern);
            else if (PatternInterfaceHelper.IsSequencePattern(patternImpl))
                SaveSequencePattern(dbPattern, (ISequencePattern)patternImpl);

            repository.SavePattern(dbPattern);
            int taskInfoId = repository.GetTaskInfoByPattern(pattern.Id);
            HttpContext.Current.Response.Redirect("add-edit-task?Mode=edit&Task=" + taskInfoId);
        }

        void SavePlainPattern(Pattern dbPattern)
        {
            dbPattern.BasicAnswer = txtBasicAnswer.Text;
            dbPattern.NextOuterPattern = null;
            dbPattern.NextInnerPattern = null;
            dbPattern.Delimiter = null;
        }

        void SaveCombiPattern(Pattern dbPattern)
        {
            // создаем новые записи
            Pattern nextPattern = null;
            for (int i = combiPatternRowsList.Count - 1; i >= 0; i--)
            {
                HtmlTableRow row = combiPatternRowsList[i];
                
                string acName = ((Label)row.Cells[0].Controls[0]).Text;
                int AnswerClassId = repository.AnswerClasses.Where(ac => ac.Name == acName).First().Id;
                string BasicAnswer = ((TextBox)row.Cells[1].Controls[0]).Text;
                string Delimiter = ((TextBox)row.Cells[2].Controls[0]).Text;
                Pattern newPattern = new Pattern()
                {
                    AnswerClassId = AnswerClassId,
                    BasicAnswer = BasicAnswer,
                    Delimiter = Delimiter,
                    NextOuterPattern = nextPattern
                };
                repository.SavePattern(newPattern);
                if (i == 0)
                    dbPattern.NextOuterPattern = newPattern;
                nextPattern = newPattern;
            }
        }

        void SaveSequencePattern(Pattern dbPattern, ISequencePattern sequence)
        {
            // создаем новые записи
            Pattern nextPattern = null;
            for (int i = sequenceAnswersTable.Rows.Count - 1; i >= 1; i--)
            {
                string basicAnswer = ((Label)sequenceAnswersTable.Rows[i].Cells[0].Controls[0]).Text;
                Pattern newPattern = new Pattern()
                {
                    AnswerClassId = seqAnswerClassId,
                    BasicAnswer = basicAnswer,
                    Delimiter = null,
                };
                repository.SavePattern(newPattern);

                if (i == 0)
                    dbPattern.NextInnerPattern = newPattern;
                newPattern.NextInnerPattern = nextPattern;
                nextPattern = newPattern;
                repository.SavePattern(newPattern);
            }
        }

        void SaveGenPattern(Pattern dbPattern)
        {
            // обновить данные в начальной записи
            SavePlainPattern(dbPattern);
            dbPattern.BasicAnswer = txtBasicAnswer.Text;
        }
        protected void DelBtn_Click(object sender, CommandEventArgs e) {
            int index = int.Parse(e.CommandArgument.ToString());
            seqPatternList.RemoveAt(index);
            sequenceAnswersTable.Rows.RemoveAt(index + 1);
        }
        protected void AddBtn_Click(object sender, EventArgs e) {
            string basicAnswer = txtBasicAnswer2.Text;
            if (string.IsNullOrEmpty(basicAnswer))
                return;
            Pattern newPattern = new Pattern
            {
                AnswerClassId = seqAnswerClassId,
                BasicAnswer = basicAnswer,
                Delimiter = null
            };
            txtBasicAnswer2.Text = "";
            seqPatternList.Add(newPattern);
            FillTableSeq();
        }
    }
}