﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentStatistic.aspx.cs" Inherits="_SmartCheckerWeb.Pages.Teacher.StudentStatistic"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">
        <h3><a href="#">Статистика по студентам</a></h3>
        <div class="shiftedBlock">
            <a href="task-statistic">По задачам</a>
            &nbsp;&nbsp;
            <a href="#">По студентам</a>
        </div>
        <br />
        <% foreach (var studentKey in studentDict.Keys)
            {
        %>
        <div class="shiftedBlock">
            <h3><a href="#"><%= studentDict[studentKey].First().User.ToString() %></a></h3>
        </div>
        <table class="styled-table">
            <thead>
                <tr>
                    <th>Задача</th>
                    <th>Результат</th>
                    <th>Попытка</th>
                </tr>
            </thead>
            <%foreach (var stat in GetStatistic(studentKey))
                {
                    Response.Write(String.Format(@"
                        <tr>
                            <td>{0}</td>
                            <td>{1}/{2}</td>
                            <td>{3}</td>
                        </tr>",
                        stat.TaskInfo.Name, stat.Result, stat.MaxResult, stat.Attempt));
                }
            %>
        </table>

        <br />
        <br />
        <%
            }
        %>
    </div>
</asp:Content>

