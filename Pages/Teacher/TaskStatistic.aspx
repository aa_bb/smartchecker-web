﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskStatistic.aspx.cs" Inherits="_SmartCheckerWeb.Pages.Teacher.TaskStatistic"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">
        <h3><a href="#">Статистика по задачам</a></h3>
        <div class="shiftedBlock">
            <a href="#">По задачам</a>
            &nbsp;&nbsp;
            <a href="student-statistic">По студентам</a>
        </div>
        <br />
        <% foreach (var taskInfoKey in taskInfoDict.Keys)
            {
        %>
        <div class="shiftedBlock">
            <h3><a href="#"><%= repository.FindTaskInfo(taskInfoDict[taskInfoKey].First().TaskInfoId).Name %></a></h3>
        </div>
        <table class="styled-table">
            <thead>
                <tr>
                    <th>Студент</th>
                    <th>Результат</th>
                    <th>Попытка</th>
                </tr>
            </thead>
            <%foreach (var stat in GetStatistic(taskInfoKey))
                {
                    Response.Write(String.Format(@"
                        <tr>
                            <td>{0} {1}</td>
                            <td>{2}/{3}</td>
                            <td>{4}</td>
                        </tr>",
                        stat.User.Surname, stat.User.Name, stat.Result, stat.MaxResult, stat.Attempt));
                }
            %>
        </table>

        <br />
        <br />
        <%
            }
        %>
    </div>
</asp:Content>

