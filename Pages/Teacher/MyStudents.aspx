﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyStudents.aspx.cs" Inherits="_SmartCheckerWeb.Pages.MyStudents"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">
        <h3><a href="#">Мои Студенты</a></h3>
        <table id="myStudentsTable" runat="server" class="styled-table">
            <thead>
                <tr>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th></th>
                </tr>
            </thead>
        </table>

        <br />
        <h3><a href="#">Добавить студентов в мою группу</a></h3>

        <table id="studentsTable" runat="server" class="styled-table">
            <thead>
                <tr>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</asp:Content>

