﻿using System;
using System.Collections.Generic;
using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;

namespace _SmartCheckerWeb.Pages
{
    public partial class MyTasksTeacher : System.Web.UI.Page
    {
        private Repository repository = new Repository();
        static List<TaskInfo> taskInfos;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AuthUtils.CheckAuth("my-tasks", Context, 2);
                int userId = int.Parse(Session["UserId"].ToString());
                taskInfos = repository.TaskInfos.Where(ti => ti.TaskOwners != null && ti.TaskOwners.Where(to => to.UserId == userId).Any()).ToList();
            }
            FillTable();
        }

        private void FillTable()
        {
            HtmlTableRow header = taskInfoTable.Rows[0];
            taskInfoTable.Rows.Clear();
            taskInfoTable.Rows.Add(header);
            foreach (var taskInfo in taskInfos)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableUtils.AddCell(row, taskInfo.Name);
                HtmlTableUtils.AddCell(row, (taskInfo.Tasks == null ? 0 : taskInfo.Tasks.Count).ToString());
                
                HtmlTableUtils.AddBtnCell(row, "Изменить", taskInfo.Id.ToString(), UpdateTask_Click);
                HtmlTableUtils.AddBtnCell(row, "Удалить", taskInfo.Id.ToString(), DeleteTask_Click);
                HtmlTableUtils.AddCell(row, taskInfo.Key);

                taskInfoTable.Rows.Add(row);
            }
        }

    protected void DeleteTask_Click(object sender, CommandEventArgs e)
        {
            int id = int.Parse(e.CommandArgument.ToString());
            int index = taskInfos.FindIndex(t => t.Id == id);
            var taskInfo = taskInfos[index];
            if (File.Exists(taskInfo.TaskDescriptionFilePath))
                File.Delete(taskInfo.TaskDescriptionFilePath);
            foreach (var task in taskInfo.Tasks)
            {
                string testPath = Path.Combine(Server.MapPath(FilePathHelper.TestsPath), taskInfo.Id.ToString(), task.Test.Name);
                if (File.Exists(testPath))
                    File.Delete(testPath);
            }
            repository.CompletelyDeleteTaskInfo(taskInfos[index]);
            taskInfos.RemoveAt(index);
            taskInfoTable.Rows.RemoveAt(index + 1);
        }

        protected void UpdateTask_Click(object sender, CommandEventArgs e)
        {
            int id = int.Parse(e.CommandArgument.ToString());
            HttpContext.Current.Response.Redirect("add-edit-task?Mode=edit&Task=" + id);
        }

        protected void AddTask_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect("add-edit-task?Mode=add");
        }
    }
}