﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditPattern.aspx.cs" Inherits="_SmartCheckerWeb.Pages.Teacher.AddEditPattern"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">
        <asp:Label runat="server" Text="Класс ответа:" />
        <asp:DropDownList ID="PatternDropDownList" Width="200px" runat="server" AutoPostBack="True"
            OnSelectedIndexChanged="PatternDropDownList_SelectedIndexChanged"/>
        <br />
        <br />

        <div id="plainGenPattern" runat="server">
            <asp:Label runat="server" Text="Базовый ответ:" AssociatedControlID="txtBasicAnswer" />
            <asp:TextBox runat="server" class="textInput" ID="txtBasicAnswer"/>
            <br />
            <br />
        </div>


        <div id="sequencePattern" runat="server">
            <table id="sequenceAnswersTable" class="styled-table" runat="server">
                <thead>
                    <tr><th>Варианты</th></tr>
                </thead>
            </table>
            
            <br /><br /><br />
            <div class="shiftedBlock">Добавить вариант ответа</div>
            <asp:Label runat="server" Text="Ответ:" AssociatedControlID="txtBasicAnswer2" />
            <asp:TextBox runat="server" class="textInput" ID="txtBasicAnswer2" /><br /><br />
            <asp:Button class="btn btn-info widebtn" ID="AddBtn" runat="server" OnClick="AddBtn_Click" Text="Добавить" />
            <br /><br />
        </div>

        <div id="combiPattern" runat="server">
            <table id="combiPatternTable" runat="server" class="styled-table">
                <thead>
                    <tr>
                        <th>Класс ответа</th>
                        <th>Базовый ответ</th>
                        <th>Разделители</th>
                    </tr>
                </thead>
            </table>
        </div>

        <asp:Button class="btn btn-info widebtn" ID="OkBtn" runat="server" OnClick="Ok_Click" Text="OK" />
    </div>
</asp:Content>
