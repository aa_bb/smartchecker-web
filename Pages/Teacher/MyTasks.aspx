﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyTasks.aspx.cs" Inherits="_SmartCheckerWeb.Pages.MyTasksTeacher"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">
        <h3><a href="#">Задачи</a></h3>
        <table class="styled-table" id="taskInfoTable" runat="server">
            <thead>
                <tr>
                    <th>Название задачи</th>
                    <th>Кол-во тестов</th>
                    <th></th>
                    <th></th>
                    <th>Ключ</th>
                </tr>
            </thead>
        </table>
        <asp:Button class="btn btn-info widebtn" ID="AddTask" runat="server" OnClick="AddTask_Click" Text="Добавить задачу" />

        <br />
        <br />

        <span style="font-family: sans-serif;">
            <asp:Label ID="label" AssociatedControlID="PrintAllKeys" Text="Распечатать ключи:" runat="server" />
        </span>
    </div>
</asp:Content>

