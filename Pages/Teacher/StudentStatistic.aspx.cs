﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages.Teacher
{
    public partial class StudentStatistic : System.Web.UI.Page
    {
        protected Repository repository = new Repository();
        protected Dictionary<int, List<Stat>> studentDict;
        static int userId;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                AuthUtils.CheckAuth("student-statistic", Context, 2);
                userId = int.Parse(Session["UserId"].ToString());
                IEnumerable<Stat> myStat = repository.Stats.Where(stat => stat.TaskInfo.TaskOwners.Where(to => to.UserId == userId).Any());
                studentDict = new Dictionary<int, List<Stat>>();
                foreach (var s in myStat)
                {
                    if (!studentDict.ContainsKey(s.UserId))
                        studentDict[s.UserId] = new List<Stat>();
                    studentDict[s.UserId].Add(s);
                }
            }
        }

        protected IEnumerable<Stat> GetStatistic(int userId)
        {
            // исключить ранние попытки, вернуть только лучшие
            Dictionary<int, List<Stat>> attemptDict = new Dictionary<int, List<Stat>>();
            List<Stat> finalStat = new List<Stat>();

            foreach (Stat stat in studentDict[userId])
            {
                if (!attemptDict.ContainsKey(stat.TaskInfoId))
                    attemptDict.Add(stat.TaskInfoId, new List<Stat>());
                attemptDict[stat.TaskInfoId].Add(stat);
            }

            foreach (var userAttempts in attemptDict.Keys)
            {
                Stat maxAttempt = attemptDict[userAttempts].Aggregate((i, j) => i.Result > j.Result ? i : j);
                finalStat.Add(maxAttempt);
            }
            return finalStat;
        }
    }
}

