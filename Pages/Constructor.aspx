﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Constructor.aspx.cs" Inherits="_SmartCheckerWeb.Pages.Constructor"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../Content/Styles.css');
    </style>
    <div class="centered_container">
        <h3><a href="#">Конструктор КПТ</a></h3>
        <br />
        <asp:Label ID="lblCUTName" Text="Название КПТ:" AssociatedControlID="txtCUTName" runat="server" />&nbsp;&nbsp;
            <asp:TextBox ID="txtCUTName" runat="server" />
        <asp:RequiredFieldValidator ID="NameRequiredValidator" runat="server" ErrorMessage="Обязательно" ControlToValidate="txtCUTName" ForeColor="Red" />
        <br />
        <br />

            <table id="CUTContainTable" class="styled-table" runat="server">
                <thead>
                    <tr><th>Классы</th></tr>
                </thead>
            </table>
        <br />
        <br />

        <asp:DropDownList ID="CUTContainClasses" runat="server"></asp:DropDownList>
        <br />
        <br />
        <asp:Button class="btn btn-info" ID="AddClassToCUTContainBtn" runat="server" OnClick="AddClassToCUTContain_Click"
            Text="Добавить" />
        <br />
        <br />
        <asp:Button class="btn btn-info" ID="OKCUTBtn" runat="server" OnClick="OKCUT_Click" Text="OK" />
    </div>
</asp:Content>
