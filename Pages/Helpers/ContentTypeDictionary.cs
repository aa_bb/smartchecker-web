﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _SmartCheckerWeb.Pages.Helpers
{
    public class ContentTypeDictionary
    {
        private static Dictionary<string, string> ContentTypeDict = new Dictionary<string, string>
        {
            { ".htm", "text/HTML" },
            { ".html", "text/HTML" },
            { ".txt", "text/plain"},
            { ".doc", "Application/msword"},
            { ".docx", "Application/msword"},
            { ".rtf",  "Application/msword"},
            { ".xls",  "Application/x-msexcel" },
            { ".xlsx", "Application/x-msexcel"},
            { ".jpg",  "image/jpeg"},
            { ".jpeg", "image/jpeg"},
            { ".gif", "image/GIF"},
            { ".pdf", "application/pdf" },
            {".zip", "application/zip"},

            {".au", "audio/basic"},
            {".avi", "video/avi"},
            { ".bmp", "image/bmp"},
            { ".bz2", "application/x-bzip2"},
            { ".css", "text/css"},
            { ".dtd", "application/xml-dtd"},
            { ".es", "application/ecmascript"},
            { ".exe", "application/octet-stream"},
            {".gz", "application/x-gzip"},
            {".hqx", "application/mac-binhex40"},
            { ".jar", "application/java-archive"},
            {".js", "application/x-javascript"},
            {".midi", "audio/x-midi"},
            { ".mp3", "audio/mpeg"},
            {".mpeg", "video/mpeg"},
            { ".ogg", "audio/vorbis"},
            { ".pdf", "application/pdf"},
            { ".pl", "application/x-perl"},
            { ".png", "image/png"},
            {".potx", "application/vnd.openxmlformats-officedocument.presentationml.template"},
            {".ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
            {".ppt", "application/vnd.ms-powerpointtd>"},
            {".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
            {".ps", "application/postscript"},
            {".qt", "video/quicktime"},
            {".ra", "audio/x-pn-realaudio"},
            {".ram", "audio/x-pn-realaudio"},
            {".rdf", "application/rdf, application/rdf+xml"},
            {".sgml", "text/sgml"},
            { ".sit", "application/x-stuffit"},
            { ".sldx", "application/vnd.openxmlformats-officedocument.presentationml.slide"},
            { ".svg", "image/svg+xml"},
            { ".swf", "application/x-shockwave-flash"},
            { ".tar.gz", "application/x-tar"},
            { ".tgz", "application/x-tar"},
            { ".tiff", "image/tiff"},
            { ".tsv", "text/tab-separated-values"},
            { ".wav", "audio/wav"},
            { ".xlam", "application/vnd.ms-excel.addin.macroEnabled.12"},
            { ".xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12"},
            { ".xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
            { ".xml", "application/xml"},
        };


        public static string GetContentType(string extension)
        {
            return ContentTypeDict[extension];
        }
    }
}