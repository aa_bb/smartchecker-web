﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _SmartCheckerWeb.Pages.Helpers
{
    public class PatternInterfaceHelper
    {
        public static bool IsPlainPattern(IPattern pat)
        {
            return pat.GetType().GetInterface(typeof(IPlainPattern).Name) != null;
        }

        public static bool IsCombiPattern(IPattern pat)
        {
            return pat.GetType().GetInterface(typeof(ICombiPattern).Name) != null;
        }
        public static bool IsSequencePattern(IPattern pat)
        {
            return pat.GetType().GetInterface(typeof(ISequencePattern).Name) != null;
        }
        public static bool IsGenPattern(IPattern pat)
        {
            return pat.GetType().GetInterface(typeof(IGenPattern).Name) != null;
        }
    }
}