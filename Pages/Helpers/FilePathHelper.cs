﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace _SmartCheckerWeb.Pages.Helpers
{
    public class FilePathHelper
    {
        public static string DescriptionPath = "~/Files/Descriptions/";
        public static string SolutionPath = "~/Files/Solutions/";
        public static string ModulePath = "~/Files/Interfaces/";
        public static string TestsPath = "~/Files/Tests/";

        public static void RelocateTest(string testName, int taskInfoId, HttpServerUtility Server)
        {
            string dirPath = string.Format("{0}{1}\\", Server.MapPath(TestsPath), taskInfoId);
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);
            string filePath = Path.Combine(Server.MapPath(TestsPath), testName);
            if (File.Exists(filePath))
                File.Move(filePath, Path.Combine(dirPath, testName));
        }

        public static string MoveDescriptionFile(string previousFilename, int taskInfoId, HttpServerUtility Server)
        {
            string descriptionFilePath = Path.Combine(Server.MapPath(DescriptionPath), previousFilename);
            string extension = Path.GetExtension(previousFilename);
            string newDescriptionFilePath = string.Format("{0}Descr{1}{2}", Server.MapPath(DescriptionPath), taskInfoId, extension);
            if (File.Exists(descriptionFilePath))
                File.Move(descriptionFilePath, newDescriptionFilePath);
            return newDescriptionFilePath;
        }

        public static void DeleteOldDescriptionFile(string descriptionFile)
        {
            if (File.Exists(descriptionFile))
                File.Delete(descriptionFile);
        }

        public static void DeleteTestFile(string testName, HttpServerUtility Server, int taskInfoId = 0)
        {
            string path;
            if (taskInfoId == 0)
            {
                path = Path.Combine(Server.MapPath(TestsPath), testName);
                if (File.Exists(path))
                    File.Delete(path);
                return;
            }
            path = Path.Combine(Server.MapPath(TestsPath), taskInfoId.ToString(), testName);
            if (File.Exists(path))
                File.Delete(path);
        }

        public static void ClearTxtFromPanDirectory(string panDirectoryPath)
        {
            var txtFiles = Directory.GetFiles(panDirectoryPath, "*.txt");
            foreach (var file in txtFiles)
                if (File.Exists(file))
                    File.Delete(file);
        }

        public static string CreatePanDirectory(int taskInfoId, int userId, string solutionPath)
        {
            string solutionDir = Path.GetDirectoryName(solutionPath);
            string panDirectoryPath = string.Format("{0}\\Pan_{1}_{2}\\", solutionDir, taskInfoId, userId);
            if (!Directory.Exists(panDirectoryPath))
                Directory.CreateDirectory(panDirectoryPath);
            return panDirectoryPath;
        }
    }
}