﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages.Helpers
{
    public class HtmlTableUtils
    {
        public static void AddCell(HtmlTableRow row, string text)
        {
            HtmlTableCell cell = new HtmlTableCell();
            var lbl = new Label() { Text = text };
            cell.Controls.Add(lbl);
            row.Cells.Add(cell);
        }

        public static void AddTextCell(HtmlTableRow row, string text)
        {
            HtmlTableCell cell = new HtmlTableCell();
            var txt = new TextBox() { Text = text, AutoPostBack = true };
            cell.Controls.Add(txt);
            row.Cells.Add(cell);
        }

        public static void AddBtnCell(HtmlTableRow row, string text, string commandArg, CommandEventHandler command)
        {
            HtmlTableCell cell = new HtmlTableCell();
            Button btn = new Button()
            {
                CssClass = "btn btn-info",
                Text = text,
                CommandArgument = commandArg,
                UseSubmitBehavior = false
            };
            btn.Command += command;
            cell.Controls.Add(btn);
            row.Cells.Add(cell);
        }
    }
}