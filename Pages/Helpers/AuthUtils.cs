﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace _SmartCheckerWeb.Pages.Helpers
{
    public class AuthUtils
    {
        public static void CheckAuth(string redirectUrl, HttpContext Context, int minLevelCode)
        {
            string url = redirectUrl.Replace('?', '&');
            if (HttpContext.Current.Session["UserId"] == null)
                HttpContext.Current.Response.Redirect("/login?RedirectUrl=" + url);

            if (int.Parse(HttpContext.Current.Session["UserRole"].ToString()) < minLevelCode)
            {
                Context.Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
                Context.Response.Flush();
                Context.Response.End();
            }
        }

        public static string ComputeSha256Hash(string rawData)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                    builder.Append(bytes[i].ToString("x2"));
                return builder.ToString();
            }
        }
    }
}