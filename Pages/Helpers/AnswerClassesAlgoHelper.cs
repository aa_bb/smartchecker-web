﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;
using _SmartCheckerWeb.Pages.Helpers.EmbeddedPattern;

namespace _SmartCheckerWeb.Pages.Helpers
{
    public class AnswerClassesAlgoHelper
    {
        public static Dictionary<string, IPattern> answerClassesDict { get; set; } = new Dictionary<string, IPattern>();
        public const int EmbeddedPluginId = 1;
        public static IPattern GetAnswerClassAlgo(AnswerClass answerClass)
        {
            if (!answerClassesDict.ContainsKey(answerClass.Name))
            {
                if (answerClass.IsCombi)
                {
                    //load from database
                    var repository = new Repository();
                    Pattern answerClassStartPattern = repository.Patterns.Where(p => p.AnswerClassId == answerClass.Id).FirstOrDefault();
                    if (answerClassStartPattern == null)
                        throw new Exception("Указанного класса ответа не существует в базе " + answerClass.Id);

                    List<IPattern> patternList = new List<IPattern>();
                    int? nextId = answerClassStartPattern.NextOuterPatternId;

                    while (nextId != null)
                    {
                        var pattern = repository.Patterns.Where(p => p.Id == nextId).FirstOrDefault();
                        if (pattern == null)
                            throw new Exception("Указанного nextId не существует в базе " + nextId);
                        IPattern patternFromLibrary = GetAnswerClassAlgo(pattern.AnswerClass);
                        patternList.Add(patternFromLibrary);
                        nextId = pattern.NextOuterPatternId;
                    }
                    CombiPattern combiPattern = new CombiPattern()
                    {
                        Name = answerClass.Name,
                        PatternList = patternList
                    };
                    answerClassesDict.Add(combiPattern.Name, combiPattern);
                }
                else
                {  // load with reflection from dll module
                    List<IPattern> loadedAnswerClasses = GetAnswerClassesFromModule(answerClass.Plugin.Path);
                    foreach (var answCl in loadedAnswerClasses)
                        if (!answerClassesDict.ContainsKey(answCl.Name))
                            answerClassesDict.Add(answCl.Name, answCl);
                }
            }
            return answerClassesDict[answerClass.Name];
        }

        public static List<IPattern> GetAnswerClassesFromModule(string modulePath)
        {
            List<IPattern> answerClasses = new List<IPattern>();
            var allTypes = Assembly.LoadFile(modulePath).GetTypes();
            foreach (var t in allTypes)
            {
                var i = t.GetInterface("IPattern");
                if (i == null)
                    continue;
                foreach (var c in t.GetConstructors())
                    if (c.GetParameters().Length == 0)
                        answerClasses.Add(c.Invoke(new object[0]) as IPattern);
            }
            return answerClasses;
        }
    }
}