﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using System.Web;
using System.Management;

namespace _SmartCheckerWeb.Pages.Helpers
{
    public class TestModule
    {
        Repository repository = new Repository();
        public bool WithErrors { get; private set; }
        public int SuccessTestCount { get; private set; } = 0;
        public int ErrorTestCount { get; private set; } = 0;
        public int RunnedTestCount { get; private set; } = 0;

        string programPath = "";
        int taskInfoId;
        int userId;
        string errors = "";
        Process currentProcess;
        string panDirectoryPath;

        List<(string testName, Pattern pattern, string studentAnswer, bool result)> testResults;

        public TestModule(string programPath, int taskInfoId, int userId)
        {
            this.programPath = programPath;
            this.taskInfoId = taskInfoId;
            this.userId = userId;
            testResults = new List<(string testName, Pattern pattern, string studentAnswer, bool result)>();
            currentProcess = null;
        }

        //Получить путь к папке с тестами и прогнать все тесты, результат записать в result
        public string TestProgram()
        {
            WithErrors = true;
            if (errors != "")
                return errors;

            TaskInfo taskInfo = repository.FindTaskInfo(taskInfoId);
            if (taskInfo == null)
            {
                errors = "Такой задачи нет";
                return errors;
            }

            if (taskInfo.Tasks.Count() == 0)
            {
                errors = "Тесты не были добавлены";
                return errors;
            }

            panDirectoryPath = FilePathHelper.CreatePanDirectory(taskInfo.Id, userId, programPath);
            // перемещаем исполняемый файл в "котел"
            string programName = Path.GetFileName(programPath);
            string newProgramPath = Path.Combine(panDirectoryPath, programName);
            if (!File.Exists(newProgramPath))
                File.Move(programPath, newProgramPath);

            foreach (var task in taskInfo.Tasks)
            { //Для каждого таска
                try
                {
                    Task dbTask = repository.FindTask(task.TaskInfoId, task.TestId);
                    CheckTestCase(dbTask.Test.Name, dbTask.Pattern, task.TimeLimit * 1000);
                    RunnedTestCount++;
                }
                catch (Exception e)
                {
                    errors += e.Message;
                }
            }

            File.Delete(Path.Combine(panDirectoryPath, programName));
            FilePathHelper.ClearTxtFromPanDirectory(panDirectoryPath);

            if (RunnedTestCount == 0)
            {
                errors = "Ни один тест не был корректно проверен";
                return errors;
            }
            WithErrors = false;
            return string.Format("{0}\\{1}", SuccessTestCount, RunnedTestCount);
        }

        private static void KillProcessAndChildren(int pid)
        {
            if (pid == 0)
                return;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher
                    ("Select * From Win32_Process Where ParentProcessID=" + pid);
            ManagementObjectCollection moc = searcher.Get();
            foreach (ManagementObject mo in moc)
            {
                KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
            }
            try
            {
                Process proc = Process.GetProcessById(pid);
                proc.Kill();
                proc.WaitForExit();
            }
            catch (ArgumentException)
            {
                // процесс уже существует
            }
        }

        void PrepareTest(string testName)
        {
            try
            {
                string testsDirectory = HttpContext.Current.Server.MapPath(FilePathHelper.TestsPath);
                string currentTestPath = Path.Combine(testsDirectory, taskInfoId.ToString(), testName);
                string targetTestPanPath = Path.Combine(panDirectoryPath, Path.GetFileName(testName));
                // копируем файл теста в "котел"
                File.Copy(currentTestPath, targetTestPanPath, true);
                // переименовываем, чтобы программа студента распознала тест
                File.Move(targetTestPanPath, Path.Combine(panDirectoryPath, "input.txt"));
            }
            catch (Exception e)
            {
                throw new Exception("Проблемы с тестом " + e.Message);
            }
        }

        void GetExecResult(int waitingTime)
        {
            string programName = Path.GetFileName(programPath);
            //Запустить программу
            currentProcess = new Process();
            currentProcess.StartInfo.FileName = Path.Combine(Path.GetFullPath(panDirectoryPath), programName);
            currentProcess.StartInfo.WorkingDirectory = Path.GetFullPath(panDirectoryPath);
            currentProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            currentProcess.StartInfo.Verb = "runas";
            currentProcess.StartInfo.UseShellExecute = true;
            currentProcess.Start();

            //Дождаться окончание выполнения прораммы
            if (waitingTime != 0)
                currentProcess.WaitForExit(waitingTime);
            else
                currentProcess.WaitForExit();
        }

        string ReadStudentAnswer(string resultFilePath)
        {
            string studentAnswer;
            try
            {
                var reader = new StreamReader(resultFilePath);
                studentAnswer = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception e)
            {
                throw new Exception("Файл не был закрыт " + e.Message);
            }
            return studentAnswer;
        }

        void FillCombiAnswerComponents(Pattern pattern, List<string> delimiters, List<List<string>> basicAnswers)
        {
            int? nextOuterPatternId = pattern.NextOuterPatternId;
            int? nextInnerPatternId = pattern.NextInnerPatternId;
            do
            {
                List<string> curBasicAnswers = new List<string>();
                Pattern curPattern = repository.FindPattern(nextOuterPatternId);
                if (curPattern != null)
                {
                    nextOuterPatternId = curPattern.NextOuterPatternId;
                    nextInnerPatternId = curPattern.NextInnerPatternId;
                    if (nextInnerPatternId == null)
                        curBasicAnswers.Add(curPattern.BasicAnswer);
                    if (!string.IsNullOrEmpty(curPattern.Delimiter)) 
                        delimiters.Add(curPattern.Delimiter);
                    
                    basicAnswers.Add(curBasicAnswers);
                }
                while (nextInnerPatternId != null)
                {
                    Pattern curInnerPattern = repository.FindPattern(nextInnerPatternId);
                    nextInnerPatternId = curInnerPattern.NextInnerPatternId;
                    curBasicAnswers.Add(curInnerPattern.BasicAnswer);
                }
            } while (nextOuterPatternId != null);
        }

        bool CheckSolution(Pattern pattern, string studentAnswer)
        {
            AnswerClass answerClass = pattern.AnswerClass;
            IPattern lexem = AnswerClassesAlgoHelper.GetAnswerClassAlgo(answerClass);
            if (lexem == null)
                throw new Exception(string.Format("Шаблон {0} не загружен, проверьте подключенные dll", answerClass));

            if (PatternInterfaceHelper.IsCombiPattern(lexem))
            {
                List<string> delimiters = new List<string>();
                List<List<string>> basicAnswers = new List<List<string>>();
                FillCombiAnswerComponents(pattern, delimiters, basicAnswers);
                List<string> parsedStudentAnswers = ParseStudentAnswer(studentAnswer, delimiters);
                return ((ICombiPattern)lexem).IsAnswerCorrect(parsedStudentAnswers, basicAnswers);
            }
            else
                return lexem.IsAnswerCorrect(new List<string>() { pattern.BasicAnswer }, new List<string>() { studentAnswer });
        }
        List<string> ParseStudentAnswer(string studentAnswer, List<string> delimiters)
        {
            List<string> parsedStudentAnswers = new List<string>();
            foreach (string d in delimiters)
            {
                char[] dels;
                if (d == "\\r\\n")
                    dels = new char[] { '\r', '\n' };
                else dels = new char[] { d[0] };
                string[] strs = studentAnswer.Split(dels);
                parsedStudentAnswers.Add(strs[0]);
                studentAnswer = studentAnswer.Remove(0, strs[0].Length);
                studentAnswer = studentAnswer.Remove(0, dels.Length);
            }
            parsedStudentAnswers.Add(studentAnswer);
            return parsedStudentAnswers;
        }
        void CheckTestCase(string testName, Pattern pattern, int waitingTime)
        {
            string log = "";
            if (currentProcess != null)
                KillProcessAndChildren(currentProcess.Id);
            FilePathHelper.ClearTxtFromPanDirectory(panDirectoryPath);

            if (pattern == null || pattern.Id == 0 || pattern.AnswerClassId == 0)
                throw new Exception("Проблема с шаблоном");

            PrepareTest(testName);

            GetExecResult(waitingTime);

            string resultFilePath = Path.Combine(panDirectoryPath, "output.txt");
            if (!File.Exists(resultFilePath))
            {
                if (waitingTime != 0) //При работе выход за время
                    log = "Выход за допустимое время работы";
                else
                    log = "Файл с результатом работы не был создан";

                testResults.Add((testName: testName, pattern: pattern, studentAnswer: log, result: false));
                RunnedTestCount++;
                throw new Exception(log);
            }

            string studentAnswer = ReadStudentAnswer(resultFilePath);

            // генерация и сравнение ответа по шаблону
            bool isAnswerCorrect = CheckSolution(pattern, studentAnswer);
            if (isAnswerCorrect)
                SuccessTestCount++;
            else
                ErrorTestCount++;

            log = StopAndDeleteSolution();
            testResults.Add((testName: testName, pattern: pattern, studentAnswer: studentAnswer + "\n" + log, result: isAnswerCorrect));
        }

        string StopAndDeleteSolution()
        {
            string log = "";
            try
            {
                currentProcess.Kill();
            }
            catch (InvalidOperationException e)
            {
                log = " InvalidOperationException " + e;
            }
            catch (Exception e)
            {
                log = string.Format(" Проблемы при завершении текущего процесса: {0}, msg = ", currentProcess.ProcessName, e.Message);
            }

            try
            { //Удалить результирующий файл
                File.Delete(Path.Combine(panDirectoryPath, "output.txt"));
            }
            catch
            {
                log += " Проблемы при удалении файла с результатом (кто-то забыл закрыть файл) ";
            }
            return log;
        }
    }
}
