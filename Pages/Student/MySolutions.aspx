﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MySolutions.aspx.cs" Inherits="_SmartCheckerWeb.Pages.Student.CheckSolution"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">

        <table class="styled-table">
            <thead>
                <tr>
                    <th>Задача</th>
                    <th>Кол-во тестов</th>
                    <th>Номер попытки</th>
                    <th>Результат</th>
                </tr>
            </thead>
            <%
                foreach (var stat in GetStatistic())
                {
                    Response.Write(String.Format(@"
                        <tr>
                            <td>{0}</td>
                            <td>{1}</td>
                            <td>{2}</td>
                            <td>{3}/{4}</td>
                         </tr>",
                        GetTI(stat).Name, GetTI(stat).Tasks == null ? 0 : GetTI(stat).Tasks.Count,
                        stat.Attempt, stat.Result, stat.MaxResult));
                }
            %>
        </table>
    </div>
</asp:Content>


