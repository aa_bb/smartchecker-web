﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoadSolution.aspx.cs" Inherits="_SmartCheckerWeb.Pages.Student.LoadSolution"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">
        <h3><a href="#">Загрузить решение к задаче <%=TaskName%></a></h3>
        <br />
        <br />

        <asp:Label ID="labelDescr" AssociatedControlID="txtDescr" Text="Условие задачи:" runat="server" /><br />
        <asp:TextBox ID="txtdescr" ReadOnly="true" class="textInput" Rows="8" TextMode="multiline" runat="server" />
        <br />
        <br />

        <asp:Button ID="LoadDescrFileBtn" Text="Загрузить файл с условием" OnClick="LoadDescrFile_Click" class="btn btn-info widebtn" runat="server" />
        <br /><br />
        <br /><br />

        <div>
            <asp:Label ID="lblSolution" runat="server" /><br />
            <asp:FileUpload ID="FileUpload1" class="btn btn-info" accept="application/octet-stream" runat="server" />
            <asp:Button class="btn btn-info" ID="UploadSolutionBtn" runat="server" Text="Загрузить решение" OnClick="UploadSolution_Click" /><br />
        </div>
        <br />
        <br />

        <asp:Button ID="CheckBtn" Text="Проверить" OnClick="CheckSolution_Click" class="btn btn-info widebtn" runat="server" />

        <div id="checkResult" class="centered_infobox" runat="server">
            <div class="inner_infobox">
                <asp:Label ID="lblCheckResult" runat="server" />
                <br /><br />
                <asp:Button class="btn btn-info" ID="OkBtn" runat="server" OnClick="Ok_Click" Text="OK" />
            </div>
        </div>
    </div>
</asp:Content>
