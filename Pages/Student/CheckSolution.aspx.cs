﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages.Student
{
    public partial class CheckSolution1 : System.Web.UI.Page
    {
        Repository repository = new Repository();
        static int userId;
        static List<TaskInfo> taskInfos;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AuthUtils.CheckAuth("check-solutions", Context, 1);
                userId = int.Parse(Session["UserId"].ToString());
                taskInfos = repository.TaskOwners.Where(tin => tin.UserId == userId).Select(to => { return to.TaskInfo; }).ToList();
            }

            FillTable();
        }

        protected void FillTable()
        {
            HtmlTableRow header = taskTable.Rows[0];
            taskTable.Rows.Clear();
            taskTable.Rows.Add(header);

            foreach (var taskInfo in taskInfos)
                AddRow(taskInfo);
        }

        private void AddRow(TaskInfo taskInfo)
        {
            HtmlTableRow row = new HtmlTableRow();
            HtmlTableUtils.AddCell(row, taskInfo.Name);
            HtmlTableUtils.AddCell(row, (taskInfo.Tasks == null ? 0 : taskInfo.Tasks.Count).ToString());
            HtmlTableUtils.AddBtnCell(row, "Открыть", taskInfo.Id.ToString(), OpenTask_Click);
            taskTable.Rows.Add(row);
        }

        protected void GetTask(object sender, EventArgs e)
        {
            string taskKey = txtKey.Text;
            if (string.IsNullOrEmpty(taskKey))
                return;
            txtKey.Text = "";
            TaskInfo newTaskInfo = repository.GetAccessWithKey(taskKey, userId);
            if (newTaskInfo == null)
                return;
            taskInfos.Add(newTaskInfo);
            AddRow(newTaskInfo);
        }

        protected void OpenTask_Click(object sender, CommandEventArgs e)
        {
            int taskInfoId = int.Parse(e.CommandArgument.ToString());
            HttpContext.Current.Response.Redirect("load-solution?Task=" + taskInfoId);
        }
    }
}