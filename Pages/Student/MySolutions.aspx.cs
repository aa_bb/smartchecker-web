﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages.Student
{
    public partial class CheckSolution : System.Web.UI.Page
    {
        Repository repository = new Repository();
        static IEnumerable<Stat> myStat;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AuthUtils.CheckAuth("my-solutions", Context, 1);
                int userId = int.Parse(Session["UserId"].ToString());
                myStat = repository.Stats.Where(stat => stat.UserId == userId);
            }
        }

        protected TaskInfo GetTI(Stat stat)
        {
            return stat.TaskInfo;
        }

        protected IEnumerable<Stat> GetStatistic()
        {
            return myStat;
        }
    }
}