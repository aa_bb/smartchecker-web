﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckSolution.aspx.cs" Inherits="_SmartCheckerWeb.Pages.Student.CheckSolution1"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">
        <h3><a href="#">Проверить решение</a></h3>
        <br />
        <table id="taskTable" runat="server" class="styled-table">
            <thead>
                <tr>
                    <th>Задача</th>
                    <th>Тестов</th>
                </tr>
            </thead>
        </table>

        <br />
        <br />
        <h3><a href="#">Запросить доступ</a></h3>
        <asp:Label ID="labelKey" AssociatedControlID="txtKey" Text="Ключ:" runat="server" />
        <asp:TextBox ID="txtKey" runat="server" class="textInput" />
        <asp:Button ID="KeyBtn" Text="Получить задачу" OnClick="GetTask" class="btn btn-info" runat="server" />

    </div>
</asp:Content>


