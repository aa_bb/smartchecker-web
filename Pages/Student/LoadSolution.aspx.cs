﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages.Student
{
    public partial class LoadSolution : System.Web.UI.Page
    {
        Repository repository = new Repository();
        protected static string TaskName;
        static TaskInfo taskInfo;
        static string solutionPath;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AuthUtils.CheckAuth("load-solution", Context, 1);
                if (Request.QueryString["Task"] == null)
                    HttpContext.Current.Response.Redirect("check-solutions");
                int taskInfoId = int.Parse(Request.QueryString["Task"].ToString());
                taskInfo = repository.FindTaskInfo(taskInfoId);
                
                TaskName = taskInfo.Name;
                txtdescr.Text = taskInfo.TaskDescriptionText;
                if (string.IsNullOrEmpty(taskInfo.TaskDescriptionFilePath))
                    LoadDescrFileBtn.Visible = false;
                lblSolution.Visible = false;
                checkResult.Visible = false;
            }   
        }
        protected void LoadDescrFile_Click(object sender, EventArgs e) {
            try
            {
                string path = taskInfo.TaskDescriptionFilePath;
                string extension = Path.GetExtension(path);
                Response.ContentType = ContentTypeDictionary.GetContentType(extension);
                Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}_{1}{2}", "Description", TaskName, extension));
                Response.TransmitFile(path);
            } catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Ошибка", "alert('Ну удалось загрузить файл')", true);
            }
            Response.End();
        }

        protected void UploadSolution_Click(object sender, EventArgs e) {
            string filename = Path.GetFileName(FileUpload1.FileName);
            if (string.IsNullOrEmpty(filename))
                return;
            solutionPath = Server.MapPath(FilePathHelper.SolutionPath) + filename;
            lblSolution.Text = "Файл решения " + filename;
            lblSolution.Visible = true;
            FileUpload1.SaveAs(solutionPath);
        }

        protected void CheckSolution_Click(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(solutionPath))
                return;
            checkResult.Visible = true;
            int userId = int.Parse(Session["UserId"].ToString());

            TestModule testModule = new TestModule(solutionPath, taskInfo.Id, userId);
            string result = testModule.TestProgram();
            if (testModule.WithErrors)
                lblCheckResult.Text = "Задача составлена некорректно, обратитесь к преподавателю \n" + result;
            else
                lblCheckResult.Text = "Проверка завершена, результат: \n" + result;

            if (!testModule.WithErrors)
                repository.SaveStatistic(userId, taskInfo.Id, currentResult: testModule.SuccessTestCount, maxResult: testModule.RunnedTestCount);
            else
                repository.SaveStatistic(userId, taskInfo.Id, currentResult: testModule.SuccessTestCount, maxResult: taskInfo.Tasks.Count);

        }

        protected void Ok_Click(object sender, EventArgs e) {
            checkResult.Visible = false;
            HttpContext.Current.Response.Redirect("check-solutions");
        }
    }
}