﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages.Admin
{
    public partial class PluginManager : System.Web.UI.Page
    {
        Repository repository = new Repository();
        static List<Plugin> plugins;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AuthUtils.CheckAuth("plugin-manager", Context, 3);
                plugins = repository.Plugins.ToList();
            }
            FillTable();
        }

        private void FillTable()
        {
            HtmlTableRow header = pluginTable.Rows[0];
            pluginTable.Rows.Clear();
            pluginTable.Rows.Add(header);
            foreach (var plugin in plugins)
                AddRow(plugin);
            
        }

        private void AddRow(Plugin plugin)
        {
            HtmlTableRow row = new HtmlTableRow();
            string pluginName = plugin.Path.Split('\\').Last();
            HtmlTableUtils.AddCell(row, pluginName);
            HtmlTableUtils.AddCell(row, plugin.ToString());
            HtmlTableUtils.AddCell(row, plugin.Enabled.ToString());

            HtmlTableUtils.AddBtnCell(row, plugin.Enabled ? "Отключить" : "Включить", plugin.Id.ToString(), ToggleEnabled_Click);
            pluginTable.Rows.Add(row);
        }

        protected void ToggleEnabled_Click(object sender, CommandEventArgs e) {
            int id = int.Parse(e.CommandArgument.ToString());
            int index = plugins.TakeWhile(p => p.Id != id).Count();
            plugins[index].Enabled = !plugins[index].Enabled;
            Button btn = (Button)sender;
            btn.Text = plugins[index].Enabled ? "Отключить" : "Включить";
            repository.ToggleEnabledPlugin(id);
            ((Label)pluginTable.Rows[index + 1].Cells[2].Controls[0]).Text = plugins[index].Enabled.ToString();
        }
        protected void UploadPlugin_Click(object sender, EventArgs e)
        {
            string filename = Path.GetFileName(FileUpload1.FileName);
            if (string.IsNullOrEmpty(filename))
                return;
            string absoluteFilepath = Server.MapPath(FilePathHelper.ModulePath) + filename;
            FileUpload1.SaveAs(absoluteFilepath);

            Plugin plugin = new Plugin() { Path = absoluteFilepath, Enabled = true };
            plugins.Add(plugin);
            repository.SavePlugin(plugin);
            
            try
            {
                foreach (var dllAnswerClass in AnswerClassesAlgoHelper.GetAnswerClassesFromModule(absoluteFilepath))
                {
                    AnswerClass answerClass = new AnswerClass() { Name = dllAnswerClass.Name, IsCombi = false, PluginId = plugin.Id };
                    repository.SaveAnswerClass(answerClass);
                }
            } catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Ошибка", "alert('Плагин не может быть загружен')", true);
            }
            AddRow(plugin);
        }
    }
}