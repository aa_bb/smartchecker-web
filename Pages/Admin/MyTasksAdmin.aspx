﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyTasksAdmin.aspx.cs" Inherits="_SmartCheckerWeb.Pages.MyTasksAdmin"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">
        
        <table class="styled-table">
            <thead>
                <tr>
                    <th>Название задачи</th>
                    <th>Кол-во тестов</th>
                    <th>Владелец</th>
                    <th>Email</th>
                </tr>
            </thead>
                <%
                    foreach (var taskOwner in GetTaskOwners())
                    {
                        var user = taskOwner.User == null ? null : taskOwner.User;
                        string userLogin = user == null ? "Не задано" : user.Name + " " + user.Surname;
                        string userEmail = user == null ? "Не задано" : user.Email;
                        Response.Write(String.Format(@"<tr>
                            <td>{0}</td>
                            <td>{1}</td>
                            <td>{2}</td>
                            <td>{3}</td></tr>",
                            taskOwner.TaskInfo.Name, taskOwner.TaskInfo.Tasks == null ? 0 : taskOwner.TaskInfo.Tasks.Count, userLogin, userEmail));
                    }
                %>
        </table>
        <br />
        <br />
    </div>
</asp:Content>


