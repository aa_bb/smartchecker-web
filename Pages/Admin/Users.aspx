﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="_SmartCheckerWeb.Pages.Admin.Users"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">
        <h3><a href="#">Пользователи</a></h3>
        <table class="styled-table">
            <thead>
                <tr>
                    <th>Логин</th>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Роль</th>
                    <th></th>
                </tr>
            </thead>
            <%
                foreach (var user in GetUsers())
                {
                    Response.Write(String.Format(@"
                        <tr>
                            <td>{0}</td>
                            <td>{1}</td>
                            <td>{2}</td>
                            <td>{3}</td>
                            <td>{4}</td>",
                        user.UserName, user.Surname, user.Name, user.Email, user.Role.ToString()));
            %>
            <td>
                <asp:Button class="btn btn-info" ID="ChangeRoleBtn" runat="server" OnClick="ChangeRole_Click"
                    Text="Сменить роль" /></td>
            <%
                    Response.Write("</tr>");
                }
            %>
        </table>

        <div id="changeRole" class="centered_infobox" runat="server">
            <div class="inner_infobox">
                <asp:DropDownList ID="RoleDropDownList" width="200px" runat="server"></asp:DropDownList>
                <br />
                <asp:Button class="btn btn-info" ID="SelectRoleBtn" runat="server" OnClick="SelectRole_Click"
                    Text="Сменить роль" />
            </div>
        </div>
    </div>
</asp:Content>


