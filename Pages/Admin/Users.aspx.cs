﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages.Admin
{
    public partial class Users : System.Web.UI.Page
    {
        Repository repository = new Repository();
        protected void Page_Load(object sender, EventArgs e)
        {
            changeRole.Visible = false;
        }

        protected void ChangeRole_Click(object sender, EventArgs e)
        {
            changeRole.Visible = true;
            RoleDropDownList.DataSource = repository.Roles;
            RoleDropDownList.DataBind();
        }

        protected void SelectRole_Click(object sender, EventArgs e)
        {
            changeRole.Visible = false;
        }

        protected IEnumerable<User> GetUsers()
        {
            return repository.Users;
        }
    }
}