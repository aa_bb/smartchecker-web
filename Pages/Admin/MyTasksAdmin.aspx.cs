﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages
{
    public partial class MyTasksAdmin : System.Web.UI.Page
    {
        private Repository repository = new Repository();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                AuthUtils.CheckAuth("task-manager", Context, 3);
        }
        protected IEnumerable<TaskOwner> GetTaskOwners()
        {
            return repository.TaskOwners;
        }
    }
}