﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PluginManager.aspx.cs" Inherits="_SmartCheckerWeb.Pages.Admin.PluginManager"
    MasterPageFile="~/Pages/SmartChecker.Master" %>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <div class="centered_container">


        <table class="styled-table" id="pluginTable" runat="server">
            <thead>
                <tr>
                    <th>Название плагина</th>
                    <th>Доступные лексемы</th>
                    <th>Доступно</th>
                    <th></th>
                </tr>
            </thead>
        </table>
        <br />
        <div>
            <asp:FileUpload ID="FileUpload1" class="btn btn-info" runat="server" />
            <asp:Button class="btn btn-info" ID="UploadPluginBtn" runat="server" Text="Загрузить плагин" OnClick="UploadPlugin_Click" /><br />
        </div>
    </div>
</asp:Content>
