﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using _SmartCheckerWeb.Pages.Helpers.EmbeddedPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages
{
    // CUT = Custom User Types
    public partial class Constructor : System.Web.UI.Page
    {
        Repository repository = new Repository();
        string NotSetStr = new EmptyPattern().Name;
        static List<string> addedClasses;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AuthUtils.CheckAuth("constructor", Context, 2);

                List<string> availableAnswerClassesDataSource = new List<string>() { NotSetStr };
                availableAnswerClassesDataSource.AddRange(
                    repository.AnswerClasses
                    .Where(ac => ac.Plugin.Enabled && !ac.IsCombi)
                    .Select((el) => { return el.ToString(); })
                    .Distinct()
                    );
                CUTContainClasses.DataSource = availableAnswerClassesDataSource;
                CUTContainClasses.DataBind();
                addedClasses = new List<string>();
            }
            FillTable();
        }

        private void FillTable()
        {
            HtmlTableRow header = CUTContainTable.Rows[0];
            CUTContainTable.Rows.Clear();
            CUTContainTable.Rows.Add(header);

            foreach (var addedClass in addedClasses)
                AddRow(addedClass);
        }

        private void AddRow(string text)
        {
            HtmlTableRow row = new HtmlTableRow();
            HtmlTableUtils.AddCell(row, text);
            HtmlTableUtils.AddBtnCell(row, "Удалить", CUTContainTable.Rows.Count.ToString(), DelClassFromCUTContain_Click);
            CUTContainTable.Rows.Add(row);
        }

        protected void AddClassToCUTContain_Click(object sender, EventArgs e)
        {
            string answerClass = CUTContainClasses.SelectedValue;
            if (NotSetStr.Equals(answerClass)) // "Не задано"
                return;
            addedClasses.Add(answerClass);
            FillTable();
        }

        protected void DelClassFromCUTContain_Click(object sender, CommandEventArgs e)
        {
            int index = int.Parse(e.CommandArgument.ToString());
            CUTContainTable.Rows.RemoveAt(index);
            addedClasses.RemoveAt(index - 1);
        }

        protected void OKCUT_Click(object sender, EventArgs e)
        {
            List<IPattern> patternList = new List<IPattern>();
            foreach (var addedClass in addedClasses)
            {
                patternList.Add(
                    AnswerClassesAlgoHelper.GetAnswerClassAlgo(new AnswerClass()
                    {
                        Name = addedClass,
                        IsCombi = false,
                        PluginId = repository.AnswerClasses.Where(ac => ac.Name.Equals(addedClass)).First().PluginId,
                        Plugin = repository.AnswerClasses.Where(ac => ac.Name.Equals(addedClass)).First().Plugin
                    }));
            }
            CombiPattern combiPattern = new CombiPattern() { Name = txtCUTName.Text, PatternList = patternList };
            repository.SaveCombiPattern(combiPattern);
            if (!AnswerClassesAlgoHelper.answerClassesDict.ContainsKey(combiPattern.Name))
                AnswerClassesAlgoHelper.answerClassesDict.Add(combiPattern.Name, combiPattern);
            else
                AnswerClassesAlgoHelper.answerClassesDict[combiPattern.Name] = combiPattern;

            if ("2".Equals(HttpContext.Current.Session["UserRole"].ToString()))
                HttpContext.Current.Response.Redirect("my-tasks");
            else if ("3".Equals(HttpContext.Current.Session["UserRole"].ToString()))
                HttpContext.Current.Response.Redirect("plugin-manager");
        }
    }
}