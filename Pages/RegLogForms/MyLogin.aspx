﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyLogin.aspx.cs" Inherits="_SmartCheckerWeb.Pages.MyLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SmartChecker</title>
</head>
<body>
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <form id="form1" runat="server">
        <div class="centered_container">
            <h3><a href="#">Вход</a></h3>
            <br />
            <br />
            <asp:Label ID="lblError" ForeColor="Red" Text="" runat="server" />
            <table class="table">
                <tr>
                    <td style="width: 10%">
                        <asp:Label ID="LoginLabel" AssociatedControlID="txtLogin" Text="Логин*" runat="server" />
                    </td>
                    <td style="width: 90%">
                        <asp:TextBox ID="txtLogin" runat="server" class="textInput" />

                        <asp:RequiredFieldValidator ID="LoginRequiredValidator" runat="server"
                            ErrorMessage="Обязательно" ControlToValidate="txtLogin" ForeColor="Red" />
                        <asp:RegularExpressionValidator
                            ID="LoginValidator" runat="server"
                            ControlToValidate="txtLogin"
                            ErrorMessage="Некорректное имя пользователя"
                            ValidationExpression="[\w| ]*"
                            ForeColor="Red" />
                        </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        <asp:Label ID="PasswordLabel" AssociatedControlID="txtPassword" Text="Пароль*" runat="server" />
                    </td>
                    <td style="width: 90%">
                        <asp:TextBox ID="txtPassword" runat="server" class="textInput" TextMode="Password"/>

                        <asp:RequiredFieldValidator ID="PasswordRequiredValidator" runat="server"
                            ErrorMessage="Обязательно" ControlToValidate="txtPassword" ForeColor="Red" />
                        <asp:RegularExpressionValidator
                            ID="PasswordValidator" runat="server"
                            ControlToValidate="txtPassword"
                            ErrorMessage="Некорректный пароль"
                            ValidationExpression='[\w| !"§$%&amp;/()=\-?\*]*'
                            ForeColor="Red" />
                    </td>
                </tr>
            </table>
            <br />

            <asp:Button ID="LoginOkBtn" Text="ОК" OnClick="Login_Click" class="btn btn-info widebtn" runat="server" />
            <br /><br/>

            <span>Впервые здесь? <a href="register">Зарегистрироваться</a></span><br/><br />
        </div>
    </form>
</body>
</html>
