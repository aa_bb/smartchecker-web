﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="_SmartCheckerWeb.Pages.RegLogForms.Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SmartChecker</title>
</head>
<body>
    <style type='text/css'>
        @import url('../../Content/Styles.css');
    </style>
    <form id="form1" runat="server">
        <div class="centered_container">
            <h3><a href="#">Регистрация</a></h3>
            <br />
            <br />
            <table class="table">
                <tr>
                    <td style="width: 10%">
                        <asp:Label ID="lblLogin" AssociatedControlID="txtNewLogin" Text="Логин*" runat="server" />
                    </td>
                    <td style="width: 90%">
                        <asp:TextBox ID="txtNewLogin" runat="server" class="textInput" />

                        <asp:RequiredFieldValidator ID="NewLoginRequiredValidator" runat="server"
                            ErrorMessage="Обязательно" ControlToValidate="txtNewLogin" ForeColor="Red" />
                        <asp:RegularExpressionValidator
                            ID="NewLoginValidator" runat="server"
                            ControlToValidate="txtNewLogin"
                            ErrorMessage="Некорректное имя пользователя"
                            ValidationExpression="[\w| ]*"
                            ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">
                        <asp:Label ID="lblPassword" AssociatedControlID="txtNewPassword" Text="Пароль*" runat="server" />
                    </td>
                    <td style="width: 90%">
                        <asp:TextBox ID="txtNewPassword" runat="server" class="textInput" TextMode="Password" />

                        <asp:RequiredFieldValidator ID="NewPasswordRequiredValidator" runat="server"
                            ErrorMessage="Обязательно" ControlToValidate="txtNewPassword" ForeColor="Red" />
                        <asp:RegularExpressionValidator
                            ID="NewPasswordValidator" runat="server"
                            ControlToValidate="txtNewPassword"
                            ErrorMessage="Некорректный пароль"
                            ValidationExpression='[\w| !"§$%&amp;/()=\-?\*]*'
                            ForeColor="Red" />
                    </td>
                </tr>

                <tr>
                    <td style="width: 10%">
                        <asp:Label ID="lblPassword2" AssociatedControlID="txtNewPassword2" Text="Повторите пароль*" runat="server" />
                    </td>
                    <td style="width: 90%">
                        <asp:TextBox ID="txtNewPassword2" runat="server" class="textInput" TextMode="Password" />

                        <asp:RequiredFieldValidator ID="NewPasswordRequiredValidator2" runat="server"
                            ErrorMessage="Обязательно" ControlToValidate="txtNewPassword2" ForeColor="Red" />
                    </td>
                </tr>

                <tr>
                    <td style="width: 10%">
                        <asp:Label ID="lblName" AssociatedControlID="txtName" Text="Имя*" runat="server" />
                    </td>
                    <td style="width: 90%">
                        <asp:TextBox ID="txtName" runat="server" class="textInput" />

                        <asp:RequiredFieldValidator ID="nameRequiredValidator" runat="server"
                            ErrorMessage="Обязательно" ControlToValidate="txtName" ForeColor="Red" />
                        <asp:RegularExpressionValidator
                            ID="nameRegularExpressionValidator" runat="server"
                            ControlToValidate="txtName"
                            ErrorMessage="Имя содержит запрещенные символы"
                            ValidationExpression="[\w| ]*"
                            ForeColor="Red" />
                    </td>
                </tr>

                <tr>
                    <td style="width: 10%">
                        <asp:Label ID="lblSurname" AssociatedControlID="txtSurname" Text="Фамилия*" runat="server" />
                    </td>
                    <td style="width: 90%">
                        <asp:TextBox ID="txtSurname" runat="server" class="textInput" />

                        <asp:RequiredFieldValidator ID="surnameRequiredValidator" runat="server"
                            ErrorMessage="Обязательно" ControlToValidate="txtSurname" ForeColor="Red" />
                        <asp:RegularExpressionValidator
                            ID="surnameRegularExpressionValidator" runat="server"
                            ControlToValidate="txtSurname"
                            ErrorMessage="Фамилия содержит запрещенные символы"
                            ValidationExpression="[\w| ]*"
                            ForeColor="Red" />
                    </td>
                </tr>

                <tr>
                    <td style="width: 10%">
                        <asp:Label ID="lblEmail" AssociatedControlID="txtEmail" Text="Email*" runat="server" />
                    </td>
                    <td style="width: 90%">
                        <asp:TextBox ID="txtEmail" runat="server" class="textInput" />

                        <asp:RequiredFieldValidator ID="emailRequiredValidator" runat="server"
                            ErrorMessage="Обязательно" ControlToValidate="txtEmail" ForeColor="Red" />
                        <asp:RegularExpressionValidator
                            ID="emailRegularExpressionValidator" runat="server"
                            ControlToValidate="txtEmail"
                            ErrorMessage="Email некорректен"
                            ValidationExpression="[\w-\.]+@([\w-]+\.)+[\w-]{2,4}"
                            ForeColor="Red" />
                    </td>
                </tr>
            </table>

            <div class="centered_container">
                <asp:RadioButtonList ID="roleButtonList" runat="server" style="width:100%">
                    <asp:ListItem Text="Я студент" Value="student" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Я преподаватель" Value="teacher"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <br /><br/>
            <asp:Button ID="RegOkBtn" Text="ОК" OnClick="RegOk_Click" class="btn btn-info widebtn" runat="server" />
            <br />
            <br />

            <span>Уже зарегистрировались? <a href="login">Войти</a></span>
        </div>
    </form>
</body>
</html>
