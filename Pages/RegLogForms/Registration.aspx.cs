﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using _SmartCheckerWeb.Pages.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _SmartCheckerWeb.Pages.RegLogForms
{
    public partial class Registration : System.Web.UI.Page
    {
        Repository repository = new Repository();
        protected void Page_Load(object sender, EventArgs e) {}

        protected void RegOk_Click(object sender, EventArgs e)
        {
            var role = repository.Roles.FirstOrDefault(r => roleButtonList.SelectedValue.ToString().Equals(r.Name));
            int roleId = role != null ? role.Id : 0;
            int roleLevelCode = role != null ? role.LevelCode : 0;

            User user = new User {
                UserName = txtNewLogin.Text,
                Password = AuthUtils.ComputeSha256Hash(txtNewPassword.Text),
                Name = txtName.Text,
                Surname = txtSurname.Text,
                Email = txtEmail.Text,
                RoleId = roleId
            };

            repository.SaveUser(user);

            Session["UserId"] = user.Id;
            Session["UserRole"] = roleLevelCode;

            if (user.Role.LevelCode == 1)
                HttpContext.Current.Response.Redirect("check-solutions");
            else if (user.Role.LevelCode == 2)
                HttpContext.Current.Response.Redirect("my-tasks");
        }
    }
}