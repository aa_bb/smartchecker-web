﻿using _SmartCheckerWeb.Models;
using _SmartCheckerWeb.Models.Repository;
using System;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Web;
using _SmartCheckerWeb.Pages.Helpers;

namespace _SmartCheckerWeb.Pages
{
    public partial class MyLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e) {}
   
        protected void Login_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid) return;

            Repository repository = new Repository();
            List<User> users = repository.Users.ToList();
            User user = users.SingleOrDefault(u => u.UserName == txtLogin.Text && u.Password == AuthUtils.ComputeSha256Hash(txtPassword.Text));
            if (user == null)
            {
                lblError.Text = "Неверное имя пользователя или пароль";
                return;
            }
            Session["UserId"] = user.Id;
            Session["UserRole"] = user.Role.LevelCode;

            if (Request.QueryString["RedirectUrl"] != null)
                HttpContext.Current.Response.Redirect(Request.QueryString["RedirectUrl"]);
            else
            {
                if (user.Role.LevelCode == 1)
                    HttpContext.Current.Response.Redirect("check-solutions");
                else if (user.Role.LevelCode == 2)
                    HttpContext.Current.Response.Redirect("my-tasks");
                else if (user.Role.LevelCode == 3)
                    HttpContext.Current.Response.Redirect("users");
            }
        }
    }
}