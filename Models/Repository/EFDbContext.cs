﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _SmartCheckerWeb.Models.Repository
{
    public class EFDbContext : DbContext
    {
        public DbSet<Action> Actions { get; set; }
        public DbSet<AnswerClass> AnswerClasses { get; set; }
        public DbSet<Object> Objects { get; set; }
        public DbSet<Pattern> Patterns { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Plugin> Plugins { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Stat> Stats { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskInfo> TaskInfos { get; set; }
        public DbSet<TaskOwner> TaskOwners { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<User> Users { get; set; }

        public bool ApplicationContext()
        {
            return Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("server=localhost;user=root;password=root;database=smart_checker;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Action>().ToTable("action");
            modelBuilder.Entity<Action>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<AnswerClass>().ToTable("answer_class");
            modelBuilder.Entity<AnswerClass>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.Name).HasColumnName("name");
                entity.Property(e => e.IsCombi).HasColumnName("is_combi");
                entity.Property(e => e.PluginId).HasColumnName("plugin_id");
            });

            modelBuilder.Entity<Object>().ToTable("object");
            modelBuilder.Entity<Object>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<Pattern>().ToTable("pattern");
            modelBuilder.Entity<Pattern>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.AnswerClassId).HasColumnName("answer_class_id");
                entity.Property(e => e.BasicAnswer).HasColumnName("basic_answer");
                entity.Property(e => e.NextInnerPatternId).HasColumnName("next_inner_pattern");
                entity.Property(e => e.NextOuterPatternId).HasColumnName("next_outer_pattern");
                entity.Property(e => e.Delimiter).HasColumnName("delimiter");
            });

            modelBuilder.Entity<Permission>().ToTable("permission");
            modelBuilder.Entity<Permission>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.ObjectId).HasColumnName("object_id");
                entity.Property(e => e.ActionId).HasColumnName("action_id");
            });

            modelBuilder.Entity<PermissionRole>().ToTable("permission_role");
            modelBuilder.Entity<PermissionRole>(entity =>
            {
                entity.Property(e => e.RoleId).HasColumnName("role_id");
                entity.Property(e => e.PermissionId).HasColumnName("permission_id");
            });

            modelBuilder.Entity<Plugin>().ToTable("plugin");
            modelBuilder.Entity<Plugin>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.Path).HasColumnName("path");
            });

            modelBuilder.Entity<Role>().ToTable("role");
            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.Name).HasColumnName("role_name");
                entity.Property(e => e.LevelCode).HasColumnName("level_code");
            });

            modelBuilder.Entity<Stat>().ToTable("stat");
            modelBuilder.Entity<Stat>(entity =>
            {
                entity.Property(e => e.UserId).HasColumnName("user_id");
                entity.Property(e => e.TaskInfoId).HasColumnName("task_info_id");
                entity.Property(e => e.Attempt).HasColumnName("attempt");
                entity.Property(e => e.Result).HasColumnName("result");
                entity.Property(e => e.MaxResult).HasColumnName("max_result");
            });

            modelBuilder.Entity<Task>().ToTable("task");
            modelBuilder.Entity<Task>(entity =>
            {
                entity.Property(e => e.TaskInfoId).HasColumnName("task_info_id");
                entity.Property(e => e.TestId).HasColumnName("test_id");
                entity.Property(e => e.PatternId).HasColumnName("pattern_id");
                entity.Property(e => e.TimeLimit).HasColumnName("time_limit");
            });

            modelBuilder.Entity<TaskInfo>().ToTable("task_info");
            modelBuilder.Entity<TaskInfo>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.Name).HasColumnName("task_name");
                entity.Property(e => e.TaskDescriptionText).HasColumnName("task_description_text");
                entity.Property(e => e.TaskDescriptionFilePath).HasColumnName("task_description_file_path");
                entity.Property(e => e.Key).HasColumnName("task_key");
                entity.Property(e => e.DefaultTimeLimit).HasColumnName("default_time_limit");
            });

            modelBuilder.Entity<TaskOwner>().ToTable("task_owner");
            modelBuilder.Entity<TaskOwner>(entity =>
            {
                entity.Property(e => e.TaskInfoId).HasColumnName("task_info_id");
                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            modelBuilder.Entity<Test>().ToTable("test");
            modelBuilder.Entity<Test>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.Name).HasColumnName("test_name");
            });

            modelBuilder.Entity<User>().ToTable("user");
            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.UserName).HasColumnName("login");
                entity.Property(e => e.Password).HasColumnName("user_password");
                entity.Property(e => e.Name).HasColumnName("user_name");
                entity.Property(e => e.Surname).HasColumnName("user_surname");
                entity.Property(e => e.Email).HasColumnName("email");
                entity.Property(e => e.RoleId).HasColumnName("role_id");
            });

            modelBuilder.Entity<PermissionRole>().HasKey(pr => new { pr.RoleId, pr.PermissionId });
            modelBuilder.Entity<Task>().HasKey(t => new { t.TaskInfoId, t.TestId });
            modelBuilder.Entity<TaskOwner>().HasKey(to => new { to.TaskInfoId, to.UserId });
            modelBuilder.Entity<Stat>().HasKey(s => new { s.TaskInfoId, s.UserId, s.Attempt });

            modelBuilder.Entity<Pattern>().HasOne(t => t.NextOuterPattern).WithMany().HasForeignKey(t => t.NextOuterPatternId);
            modelBuilder.Entity<Pattern>().HasOne(t => t.NextInnerPattern).WithMany().HasForeignKey(t => t.NextInnerPatternId);
        }
    }
}