﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using _SmartCheckerWeb.Pages.Helpers;

namespace _SmartCheckerWeb.Models.Repository
{
    public class Repository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<Action> Actions { get { return context.Actions; } }
        public IEnumerable<AnswerClass> AnswerClasses
        {
            get
            {
                return context.AnswerClasses
                    .Include(ac => ac.Plugin);
            }
        }
        public IEnumerable<Object> Objects { get { return context.Objects; } }
        public IEnumerable<Pattern> Patterns
        {
            get
            {
                return context.Patterns
                    .Include(p => p.AnswerClass).ThenInclude(ac => ac.Plugin)
                    .Include(p => p.NextInnerPattern)
                    .Include(p => p.NextOuterPattern);
            }
        }
        public IEnumerable<Permission> Permissions { get { return context.Permissions; } }
        public IEnumerable<Plugin> Plugins
        {
            get
            {
                return context.Plugins
                    .Include(p => p.AnswerClasses);
            }
        }
        public IEnumerable<Role> Roles { get { return context.Roles; } }
        public IEnumerable<Stat> Stats
        {
            get
            {
                return context.Stats
                    .Include(s => s.User)
                    .Include(s => s.TaskInfo).ThenInclude(ti => ti.TaskOwners)
                    .Include(s => s.TaskInfo).ThenInclude(ti => ti.Tasks);
            }
        }
        public IEnumerable<Task> Tasks
        {
            get
            {
                return context.Tasks
                    .Include(t => t.Pattern).ThenInclude(p => p.AnswerClass).ThenInclude(ac => ac.Plugin)
                    .Include(t => t.Test)
                    .Include(t => t.TaskInfo);
            }
        }
        public IEnumerable<TaskInfo> TaskInfos
        {
            get
            {
                return context.TaskInfos
                    .Include(ti => ti.TaskOwners)
                    .Include(t => t.Tasks);
            }
        }
        public IEnumerable<TaskOwner> TaskOwners
        {
            get
            {
                return context.TaskOwners
                    .Include(to => to.TaskInfo).ThenInclude(ti => ti.Tasks)
                    .Include(to => to.User);
            }
        }
        public IEnumerable<Test> Tests { get { return context.Tests; } }
        public IEnumerable<User> Users
        {
            get
            {
                return context.Users
                    .Include(u => u.Role)
                    .Include(u => u.TaskOwners);
            }
        }

        public TaskInfo FindTaskInfo(int id)
        {
            return TaskInfos.FirstOrDefault(v => v.Id == id);
        }

        public Task FindTask(int taskInfoId, int testId)
        {
            return Tasks.FirstOrDefault(v => v.TaskInfoId == taskInfoId && v.TestId == testId);
        }

        public void SaveTaskInfo(TaskInfo taskInfo)
        {
            if (taskInfo.Id == 0)
                context.TaskInfos.Add(taskInfo);
            else
            {
                TaskInfo dbTaskInfo = context.TaskInfos.Find(taskInfo.Id);
                if (dbTaskInfo != null)
                {
                    dbTaskInfo.Name = taskInfo.Name;
                    dbTaskInfo.Key = taskInfo.Key;
                    dbTaskInfo.DefaultTimeLimit = taskInfo.DefaultTimeLimit;
                    dbTaskInfo.TaskDescriptionText = taskInfo.TaskDescriptionText;
                    dbTaskInfo.TaskDescriptionFilePath = taskInfo.TaskDescriptionFilePath;
                }
            }
            context.SaveChanges();
        }

        public void SaveUser(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
        }

        public User FindUser(int id)
        {
            return Users.FirstOrDefault(v => v.Id == id); ;
        }

        public void SaveTest(Test test)
        {
            if (test.Id == 0)
                context.Tests.Add(test);
            else
            {
                Test dbTest = context.Tests.Find(test.Id);
                if (dbTest != null)
                    dbTest.Name = test.Name;
            }
            context.SaveChanges();
        }

        public void SavePattern(Pattern pattern)
        {
            if (pattern.Id == 0)
                context.Patterns.Add(pattern);
            else
            {
                Pattern dbPattern = context.Patterns.Find(pattern.Id);
                if (dbPattern != null)
                {
                    dbPattern.AnswerClassId = pattern.AnswerClassId;
                    dbPattern.BasicAnswer = pattern.BasicAnswer;
                    dbPattern.NextInnerPatternId = pattern.NextInnerPatternId;
                    dbPattern.NextOuterPatternId = pattern.NextOuterPatternId;
                    dbPattern.Delimiter = pattern.Delimiter;
                }
            }
            context.SaveChanges();
        }

        public void SaveTask(Task task)
        {
            Task dbTask = context.Tasks.Find(task.TaskInfoId, task.TestId);
            if (dbTask != null)
            {
                dbTask.PatternId = task.PatternId;
                dbTask.TimeLimit = task.TimeLimit;
            }
            else
                context.Add(task);
            context.SaveChanges();
        }

        public void AddTaskOwner(int userId, int taskInfoId)
        {
            context.TaskOwners.Add(new TaskOwner { UserId = userId, TaskInfoId = taskInfoId });
            context.SaveChanges();
        }

        public void CompletelyDeleteTaskInfo(TaskInfo taskInfo)
        {
            if (taskInfo.Tasks != null)
            {
                foreach (var task in taskInfo.Tasks)
                {
                    Pattern pattern = context.Patterns.Find(task.PatternId);
                    Test test = context.Tests.Find(task.TestId);
                    context.Entry(task).State = EntityState.Deleted;
                    if (pattern != null)
                        context.Entry(pattern).State = EntityState.Deleted;
                    context.Entry(test).State = EntityState.Deleted;
                }
            }

            var taskOwners = context.TaskOwners.Where(to => to.TaskInfoId == taskInfo.Id);
            foreach (var taskOwner in taskOwners)
                context.Entry(taskOwner).State = EntityState.Deleted;

            context.Entry(taskInfo).State = EntityState.Deleted;
            context.SaveChanges();
        }

        public void SavePlugin(Plugin plugin)
        {
            if (plugin.Id == 0)
                context.Plugins.Add(plugin);
            else
            {
                Plugin dbPlugin = context.Plugins.Find(plugin.Id);
                if (dbPlugin != null)
                {
                    dbPlugin.Path = plugin.Path;
                    dbPlugin.Enabled = plugin.Enabled;
                }
            }
            context.SaveChanges();
        }

        public void ToggleEnabledPlugin(int id)
        {
            var plugin = context.Plugins.Find(id);
            plugin.Enabled = !plugin.Enabled;
            context.SaveChanges();
        }

        public void SaveAnswerClass(AnswerClass answerClass)
        {
            if (answerClass.Id == 0)
                context.AnswerClasses.Add(answerClass);
            else
            {
                AnswerClass dbAnswerClass = context.AnswerClasses.Find(answerClass.Id);
                if (dbAnswerClass != null)
                {
                    dbAnswerClass.Name = answerClass.Name;
                    dbAnswerClass.IsCombi = answerClass.IsCombi;
                    dbAnswerClass.PluginId = answerClass.PluginId;
                }
            }
            context.SaveChanges();
        }

        public void SaveCombiPattern(ICombiPattern combiPattern)
        {
            AnswerClass answerClass = new AnswerClass()
            {
                IsCombi = true,
                Name = combiPattern.Name,
                PluginId = AnswerClassesAlgoHelper.EmbeddedPluginId
            };
            context.AnswerClasses.Add(answerClass);
            context.SaveChanges();

            int? nextId = null;
            List<IPattern> patternList = combiPattern.PatternList;
            for (int i = patternList.Count - 1; i >= 0; i--)
            {
                string name = patternList[i].Name;
                AnswerClass pluginAnswerClass = context.AnswerClasses.Where(c => c.Name == name).First();
                Pattern newPattern = new Pattern() { AnswerClass = pluginAnswerClass, NextOuterPatternId = nextId };
                context.Patterns.Add(newPattern);
                context.SaveChanges();
                nextId = newPattern.Id;
            }
            Pattern startPattern = new Pattern() { AnswerClass = answerClass, NextOuterPatternId = nextId };
            context.Patterns.Add(startPattern);
            context.SaveChanges();
        }

        public Pattern FindPattern(int? id)
        {
            if (id == null)
                return null;
            return Patterns.FirstOrDefault(v => v.Id == id);
        }

        public void DeleteOldPatternRecords(Pattern pattern)
        {
            int? nextOuterPatternId = pattern.NextOuterPatternId;
            int? nextInnerPatternId = pattern.NextInnerPatternId;
            do
            {
                while (nextInnerPatternId != null)
                {
                    Pattern curInnerPattern = FindPattern(nextInnerPatternId);
                    nextInnerPatternId = curInnerPattern.NextInnerPatternId;
                    context.Patterns.Remove(curInnerPattern);
                }
                Pattern curPattern = FindPattern(nextOuterPatternId);
                if (curPattern != null)
                {
                    nextOuterPatternId = curPattern.NextOuterPatternId;
                    context.Patterns.Remove(curPattern);
                }
            } while (nextOuterPatternId != null);
        }

        public int GetTaskInfoByPattern(int patternId)
        {
            return context.Tasks.Where(t => t.PatternId == patternId).First().TaskInfoId;
        }

        public TaskInfo GetAccessWithKey(string taskKey, int userId)
        {
            TaskInfo taskInfo = context.TaskInfos.Where(ti => ti.Key.Equals(taskKey)).FirstOrDefault();
            if (taskInfo == null)
                return null;
            if (context.TaskOwners.Find(taskInfo.Id, userId) != null)
                return null;
            context.TaskOwners.Add(new TaskOwner() { UserId = userId, TaskInfoId = taskInfo.Id });
            context.SaveChanges();
            return taskInfo;
        }

        public void SaveStatistic(int userId, int taskInfoId, int currentResult, int maxResult)
        {
            IEnumerable<Stat> stats = context.Stats.Where(s => s.UserId == userId && s.TaskInfoId == taskInfoId);
            int attempt = stats.Count() == 0 ? 1 : stats.Max(s => s.Attempt) + 1;
            Stat stat = new Stat() { UserId = userId, TaskInfoId = taskInfoId, Result = currentResult, MaxResult = maxResult, Attempt = attempt };
            context.Stats.Add(stat);
            context.SaveChanges();
        }

        public void SaveTaskOwner(int taskInfoId, int userId)
        {
            if (context.TaskOwners.Find(taskInfoId, userId) == null)
            {
                var to = new TaskOwner() { TaskInfoId = taskInfoId, UserId = userId };
                context.TaskOwners.Add(to);
                context.SaveChanges();
            }
        }

        public void UpdateRole(int updateUserId, int levelCode)
        {
            User user = context.Users.Find(updateUserId);
            if (user == null)
                return;
            user.RoleId = Roles.Where(r => r.LevelCode == levelCode + 1).First().Id;
            context.SaveChanges();
        }
    }
}