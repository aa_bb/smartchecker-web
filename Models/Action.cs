﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _SmartCheckerWeb.Models
{
    public class Action
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}