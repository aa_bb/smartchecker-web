﻿using System.Linq;
using System.Text;
using _SmartCheckerWeb.Models.Repository;

namespace _SmartCheckerWeb.Models
{
    public class Pattern
    {
        public virtual AnswerClass AnswerClass { get; set; }
        public string BasicAnswer { get; set; }

        public string Delimiter { get; set; }
        public int Id { get; set; }

        public int? AnswerClassId { get; set; }

        public int? NextInnerPatternId { get; set; }

        public int? NextOuterPatternId { get; set; }

        public virtual Pattern NextOuterPattern { get; set; }

        public virtual Pattern NextInnerPattern { get; set; }

        public override string ToString()
        {
            if (Id == 0 || AnswerClass == null)
                return "Не задано";
            return string.Format("\"{0}\" \"{1}\"", AnswerClass.Name, BasicAnswer);
        }
    }
}
