﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _SmartCheckerWeb.Models
{
    public class Permission
    {
        public int Id { get; set; }
        public int ObjectId { get; set; }
        public virtual Object Object { get; set; }
        public int ActionId { get; set; }
        public virtual Action Action { get; set; }
    }
}