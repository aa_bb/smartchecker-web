﻿using System.Collections.Generic;
using System.Text;

namespace _SmartCheckerWeb.Models
{
    public class Plugin
    {
        public int Id { get; set; }
        public string Path { get; set; }
        public bool Enabled { get; set; }
        public virtual List<AnswerClass> AnswerClasses { get; set; }

        public override string ToString()
        {
            if (AnswerClasses == null)
                return "Нет";
            StringBuilder sb = new StringBuilder();
            
            foreach (var ac in AnswerClasses)
                if (sb.Length <= 50)
                sb.Append(ac.ToString() + "; ");
            return sb.ToString();
        }
    }
}

