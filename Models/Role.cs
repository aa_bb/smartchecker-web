﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _SmartCheckerWeb.Models
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int LevelCode { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}