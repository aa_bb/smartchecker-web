﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _SmartCheckerWeb.Models
{
    public class TaskOwner : IEquatable<TaskOwner>
    {
        public int TaskInfoId { get; set; }
        public virtual TaskInfo TaskInfo { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public bool Equals(TaskOwner other)
        {
            if (other is null)
                return false;

            return TaskInfoId == other.TaskInfoId && UserId == other.UserId;
        }

        public override bool Equals(object obj) => Equals(obj as TaskOwner);
        public override int GetHashCode() => (TaskInfoId, UserId).GetHashCode();
    }
}