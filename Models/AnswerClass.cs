﻿namespace _SmartCheckerWeb.Models
{
    public class AnswerClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsCombi { get; set; }

        public int? PluginId { get; set; }

        public virtual Plugin Plugin { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
