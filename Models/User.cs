﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _SmartCheckerWeb.Models
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }
        public virtual List<Permission> Permissions { get; set; }
        public virtual List<TaskOwner> TaskOwners { get; set; }

        public override string ToString()
        {
            return Surname + " " + Name;
        }
    }
}