﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _SmartCheckerWeb.Models
{
    public class TaskInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TaskDescriptionText { get; set; }
        public string TaskDescriptionFilePath { get; set; }

        private string key;
        public string Key { get { return key; } set { key = Guid.NewGuid().ToString(); } }

        public int DefaultTimeLimit { get; set; }
        public virtual List<Task> Tasks { get; set; }
        public virtual List<TaskOwner> TaskOwners { get; set; }
    }
}
