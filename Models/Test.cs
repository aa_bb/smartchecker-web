﻿namespace _SmartCheckerWeb.Models
{
    public class Test
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
