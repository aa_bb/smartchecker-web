﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _SmartCheckerWeb.Models
{
    public class Stat
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int TaskInfoId { get; set; }
        public virtual TaskInfo TaskInfo { get; set; }
        public int Attempt { get; set; }
        public int Result { get; set; }
        public int MaxResult { get; set; }
    }
}