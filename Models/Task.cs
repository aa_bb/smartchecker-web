﻿using System.Collections.Generic;
namespace _SmartCheckerWeb.Models
{
    public class Task
    {
        public virtual Test Test { get; set; }
        public virtual Pattern Pattern { get; set; }
        public int TimeLimit { get; set; }
        public int TaskInfoId { get; set; }
        public virtual TaskInfo TaskInfo { get; set; }
        public int TestId { get; set; }
        public int? PatternId { get; set; }
    }
}
