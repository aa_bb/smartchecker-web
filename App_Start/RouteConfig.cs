﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace _SmartCheckerWeb.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute(null, "login", "~/Pages/RegLogForms/MyLogin.aspx");
            routes.MapPageRoute(null, "register", "~/Pages/RegLogForms/Registration.aspx");

            // student
            routes.MapPageRoute(null, "check-solutions", "~/Pages/Student/CheckSolution.aspx");
            routes.MapPageRoute(null, "my-solutions", "~/Pages/Student/MySolutions.aspx");
            routes.MapPageRoute(null, "load-solution", "~/Pages/Student/LoadSolution.aspx");

            // teacher
            routes.MapPageRoute(null, "task-statistic", "~/Pages/Teacher/TaskStatistic.aspx");
            routes.MapPageRoute(null, "student-statistic", "~/Pages/Teacher/StudentStatistic.aspx");
            routes.MapPageRoute(null, "my-tasks", "~/Pages/Teacher/MyTasks.aspx");
            routes.MapPageRoute(null, "my-students", "~/Pages/Teacher/MyStudents.aspx");
            routes.MapPageRoute(null, "add-edit-pattern", "~/Pages/Teacher/AddEditPattern.aspx");
            routes.MapPageRoute(null, "my-colleague", "~/Pages/Teacher/MyСolleague.aspx");
            routes.MapPageRoute(null, "constructor", "~/Pages/Constructor.aspx");
            routes.MapPageRoute(null, "add-edit-task", "~/Pages/Teacher/AddEditTask.aspx");

            // admin
            routes.MapPageRoute(null, "users", "~/Pages/Admin/Users.aspx");
            routes.MapPageRoute(null, "task-manager", "~/Pages/Admin/MyTasksAdmin.aspx");
            routes.MapPageRoute(null, "plugin-manager", "~/Pages/Admin/PluginManager.aspx");
        }
    }
}